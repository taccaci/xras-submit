/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.PermissionType;

/**
 * @author mrhanlon
 *
 */
public class PermissionTypeTest {

	@Test
	public void validateUrl() {
		PermissionType fa = new PermissionType();
		Assert.assertEquals("BaseUrl is correct", PermissionType.BASE_URL + PermissionType.TYPE_URL, fa.getBaseUrl());
	}

}
