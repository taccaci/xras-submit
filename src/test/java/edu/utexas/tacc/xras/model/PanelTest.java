/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class PanelTest {

	@Test
	public void validateUrl() {
		Panel model = new Panel();
		Assert.assertEquals("BaseUrl is correct", Panel.BASE_URL, model.getBaseUrl());
	}

}
