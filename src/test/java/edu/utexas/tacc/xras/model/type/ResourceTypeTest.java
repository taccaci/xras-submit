/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.ResourceType;

/**
 * @author mrhanlon
 *
 */
public class ResourceTypeTest {

	@Test
	public void validateUrl() {
		ResourceType type = new ResourceType();
		Assert.assertEquals("BaseUrl is correct", ResourceType.BASE_URL + type.getTypeUrl(), type.getBaseUrl());
	}

}
