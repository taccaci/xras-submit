/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class ResourceTest {
	
	@Test
	public void validateUrl() {
		Resource fa = new Resource();
		Assert.assertEquals("BaseUrl is correct", Resource.BASE_URL, fa.getBaseUrl());
	}

}
