/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.AttributeSetType;

/**
 * @author mrhanlon
 *
 */
public class AttributeSetTypeTest {

	@Test
	public void validateUrl() {
		AttributeSetType fa = new AttributeSetType();
		Assert.assertEquals("BaseUrl is correct", AttributeSetType.BASE_URL + AttributeSetType.TYPE_URL, fa.getBaseUrl());
	}

}
