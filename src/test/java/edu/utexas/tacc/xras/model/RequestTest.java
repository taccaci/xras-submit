/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.model.parser.XrasParser;
import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author mrhanlon
 *
 */
public class RequestTest {

	public JsonNode mockRequest(String source) throws JsonProcessingException, IOException {
		InputStream in = getClass().getResourceAsStream(source);
		JsonNode parsed = new ObjectMapper().readTree(in);
		return parsed;
	}

	@Test
	public void validateUrl() {
		Request model = new Request();
		Assert.assertEquals("BaseUrl is correct", Request.BASE_URL, model.getBaseUrl());
	}

	@Test
	public void testSupportedByGrantsField() throws IOException {
		Request request = XrasParser.parseModel(mockRequest("/json/fixtures/request.json"), Request.class);
		Assert.assertNotNull(request);
		Assert.assertTrue("Request is supported by grants", request.isSupportedByGrants().booleanValue());
	}

}
