/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.ActionType;

/**
 * @author mrhanlon
 *
 */
public class ActionTypeTest {

	@Test
	public void validateUrl() {
		ActionType fa = new ActionType();
		Assert.assertEquals("BaseUrl is correct", ActionType.BASE_URL + ActionType.TYPE_URL, fa.getBaseUrl());
	}

}
