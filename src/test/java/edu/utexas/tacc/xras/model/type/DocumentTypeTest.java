/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.DocumentType;

/**
 * @author mrhanlon
 *
 */
public class DocumentTypeTest {

	@Test
	public void validateUrl() {
		DocumentType fa = new DocumentType();
		Assert.assertEquals("BaseUrl is correct", DocumentType.BASE_URL + DocumentType.TYPE_URL, fa.getBaseUrl());
	}

}
