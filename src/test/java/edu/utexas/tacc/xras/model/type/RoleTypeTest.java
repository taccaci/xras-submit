/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.RoleType;

/**
 * @author mrhanlon
 *
 */
public class RoleTypeTest {

	@Test
	public void validateUrl() {
		RoleType fa = new RoleType();
		Assert.assertEquals("BaseUrl is correct", RoleType.BASE_URL + RoleType.TYPE_URL, fa.getBaseUrl());
	}

}
