/**
 * 
 */
package edu.utexas.tacc.xras.service;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestReview;
import edu.utexas.tacc.xras.service.RequestReviewService;
import edu.utexas.tacc.xras.service.RequestService;

/**
 * @author mrhanlon
 *
 */
public class RequestReviewTest extends BaseXrasTest {
	
	@Autowired
	private RequestService requestService;
	
	@Autowired
	private RequestReviewService requestReviewService;
	
	@Test
	public void getReviews() throws ServiceException {
		Request request = requestService.read(session, new Request(35));
		
		RequestReview reviews = requestReviewService.get(session, request);
	
		Assert.assertEquals("Correct RequestAction objects exist in RequestReview", request.getActions().size(), reviews.getActionReviews().size());
	}

}
