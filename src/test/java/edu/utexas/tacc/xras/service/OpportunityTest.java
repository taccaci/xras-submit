/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.service.OpportunityService;

/**
 * @author mrhanlon
 *
 */
public class OpportunityTest extends BaseXrasTest {

	@Autowired
	private OpportunityService opportunityService;

	@Test
	public void read() throws ServiceException {
		Opportunity read = opportunityService.read(session, new Opportunity(1));
		Assert.assertEquals(1, read.getId());
		getLogger().debug(read.toJSON());
	}

	@Test
	public void readList() throws ServiceException {
		List<Opportunity> models = opportunityService.list(session);
		for (Opportunity model : models) {
			String state = model.getOpportunityStates().size() > 0 ? model.getOpportunityStates().get(0) : "DEFAULT";
			getLogger().debug(model.getOpportunityName() + ": " + state);
		}
	}

}
