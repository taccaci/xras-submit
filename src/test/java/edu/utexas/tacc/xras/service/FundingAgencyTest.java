/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.FundingAgency;
import edu.utexas.tacc.xras.service.FundingAgencyService;

/**
 * @author mrhanlon
 *
 */
public class FundingAgencyTest extends BaseXrasTest {
	
	@Autowired
	private FundingAgencyService faService;
	
	@Test
	public void list() throws ServiceException {
		List<FundingAgency> models = faService.list(session);
		for (FundingAgency fa : models) {
			getLogger().debug(fa.getFundingAgencyName());
		}
		Assert.assertTrue(models.size() > 0);
	}

}
