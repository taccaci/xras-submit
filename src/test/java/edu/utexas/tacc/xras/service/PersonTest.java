/**
 *
 */
package edu.utexas.tacc.xras.service;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.service.PersonService;

/**
 * @author mrhanlon
 *
 */
public class PersonTest extends BaseXrasTest {
	
	@Autowired
	private PersonService personService;

	@Test
	public void read() throws ServiceException, JsonProcessingException {
		Person read = personService.read(session, new Person("mrhanlon"));
		Assert.assertEquals("mrhanlon", read.getUsername());
	}

	@Test
	public void addUpdate() throws ServiceException, JsonProcessingException {
		Person model = new Person();
		model.setUsername("mrhanlon");
		model.setFirstName("Matthew");
		model.setMiddleName("Rollins");
		model.setLastName("Hanlon");
		model.setEmail("mrhanlon@tacc.utexas.edu");
		model.setPhone("512-232-5165");
		Person saved = personService.save(session, model);
		getLogger().info(saved.toJSON().toString());

		Assert.assertEquals(model.getUsername(), saved.getUsername());
		Assert.assertEquals(model.getFirstName(), saved.getFirstName());
		Assert.assertEquals(model.getMiddleName(), saved.getMiddleName()); // TODO funny business here
		Assert.assertEquals(model.getLastName(), saved.getLastName());
		Assert.assertEquals(model.getEmail(), saved.getEmail());
		Assert.assertEquals(model.getPhone(), saved.getPhone());
	}

}
