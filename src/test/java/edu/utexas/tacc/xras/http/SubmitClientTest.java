/**
 * 
 */
package edu.utexas.tacc.xras.http;

import java.io.IOException;

import junit.framework.Assert;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.SubmitClient;
import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
public class SubmitClientTest extends BaseXrasTest {
	
	@Autowired
	SubmitClient client;
	
	@Test
	public void readAndVerifyPropertiesStream() throws IOException {
		Assert.assertEquals("Configuration were read and the service URI was assembled correctly,"
			, "https://xras-submit-api-test.xsede.org/v1"
			, client.buildUri(""));
	}
	
	@Test
	public void invalidAllocationProcess() throws ClientProtocolException, IOException {
		SubmitClient client = new SubmitClient(
			"xras-submit-api-test.xsede.org"
			, ""
			, "/v1"
			, "INVALID" // invalid process
			, "submit"
			, "xsede-111"
			, true);
		XrasSession xsess = new XrasSession("mrhanlon");
		ApiResponse response = client.get("/requests", xsess);
		Assert.assertEquals("Missing/Invalid header XA-ALLOCATIONS-PROCESS, HTTP 401 response", 401, response.getStatusCode());
	}
	
	@Test
	public void invalidApiKey() throws ClientProtocolException, IOException {
		SubmitClient client = new SubmitClient(
			"xras-submit-api-test.xsede.org"
			, ""
			, "/v1"
			, "XSEDE"
			, "submit"
			, "invalid-api-key" // invalid api key
			, true);
		XrasSession xsess = new XrasSession("mrhanlon");
		ApiResponse response = client.get("/requests", xsess);
		Assert.assertEquals("Missing/Invalid header XA-API-KEY, HTTP 401 response", 401, response.getStatusCode());
	}
	
	@Test
	public void invalidContext() throws ClientProtocolException, IOException {
		SubmitClient client = new SubmitClient(
			"xras-submit-api-test.xsede.org"
			, ""
			, "/v1"
			, "XSEDE"
			, "invalid" // invalid context
			, "xsede-111"
			, true);
		XrasSession xsess = new XrasSession("mrhanlon");
		ApiResponse response = client.get("/requests", xsess);
		Assert.assertEquals("Missing/Invalid header XA-CONTEXT, HTTP 401 response", 401, response.getStatusCode());
	}
	
	@Test
	public void missingSession() throws ClientProtocolException, IOException {
		SubmitClient client = new SubmitClient(
			"xras-submit-api-test.xsede.org"
			, ""
			, "/v1"
			, "XSEDE"
			, "submit"
			, "xsede-111"
			, true);
		XrasSession xsess = new XrasSession("");
		ApiResponse response = client.get("/requests", xsess);
		Assert.assertEquals("Missing/Invalid header XA-USER, HTTP 400 response", 400, response.getStatusCode());
	}
  
  @Test
  public void successfulConnection() throws ClientProtocolException, IOException {
    SubmitClient client = new SubmitClient(
      "xras-submit-api-test.xsede.org"
      , ""
      , "/v1"
      , "XSEDE"
      , "submit"
      , "xsede-111"
      , true);
    XrasSession xsess = new XrasSession("mrhanlon");
    ApiResponse response = client.get("/requests", xsess);
    Assert.assertEquals("HTTP 200 response", 200, response.getStatusCode());
    getLogger().info(response.getResult());
  }
  
  @Test
  public void notFound() throws ClientProtocolException, IOException {
    SubmitClient client = new SubmitClient(
      "xras-submit-api-test.xsede.org"
      , ""
      , "/v1"
      , "XSEDE"
      , "submit"
      , "xsede-111"
      , true);
    XrasSession xsess = new XrasSession("mrhanlon");
    ApiResponse response = client.get("/opportunities/0", xsess);
    Assert.assertEquals("HTTP 404 response", 404, response.getStatusCode());
  }
}
