/**
 * 
 */
package edu.utexas.tacc.xras;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
@ContextConfiguration("/xras-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BaseXrasTest {

	protected XrasSession session;
	
	/**
	 * Configures tests to run within current user context
	 */
	@Before
	public void before() {
	  String user = System.getProperty("user.name");
		session = new XrasSession(user);
	}
	
	@After
	public void after() {
		session = null;
	}

	private Logger logger;
	
	protected Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}
}
