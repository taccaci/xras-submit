/**
 * 
 */
package edu.utexas.tacc;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.BaseXrasModel;
import edu.utexas.tacc.xras.model.FundingAgency;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.model.Panel;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestReview;
import edu.utexas.tacc.xras.model.Resource;
import edu.utexas.tacc.xras.model.Type;
import edu.utexas.tacc.xras.service.FundingAgencyService;
import edu.utexas.tacc.xras.service.OpportunityService;
import edu.utexas.tacc.xras.service.PanelService;
import edu.utexas.tacc.xras.service.PersonService;
import edu.utexas.tacc.xras.service.RequestReviewService;
import edu.utexas.tacc.xras.service.RequestService;
import edu.utexas.tacc.xras.service.ResourceService;
import edu.utexas.tacc.xras.service.type.ActionTypeService;
import edu.utexas.tacc.xras.service.type.AllocationDateTypeService;
import edu.utexas.tacc.xras.service.type.AttributeSetRelationTypeService;
import edu.utexas.tacc.xras.service.type.AttributeSetTypeService;
import edu.utexas.tacc.xras.service.type.DocumentTypeService;
import edu.utexas.tacc.xras.service.type.FieldOfScienceTypeService;
import edu.utexas.tacc.xras.service.type.OpportunityStateTypeService;
import edu.utexas.tacc.xras.service.type.PermissionTypeService;
import edu.utexas.tacc.xras.service.type.RequestStatusTypeService;
import edu.utexas.tacc.xras.service.type.RequestTypeService;
import edu.utexas.tacc.xras.service.type.ResourceNumberTypeService;
import edu.utexas.tacc.xras.service.type.ResourceTypeService;
import edu.utexas.tacc.xras.service.type.RoleTypeService;
import edu.utexas.tacc.xras.service.type.UnitTypeService;

/**
 * @author mrhanlon
 *
 */
public class GenerateFixtures {

  /**
   * This class generates test fixtures. It should only be run when the Submit API
   * data model changes.
   * @param args
   *    Arguments passed to this method are the xsede username with which to generate the fixture data.
   *    API access configuration is managed by <code>src/test/resources/xras-submit.xml</code>.
   * @throws ServiceException 
   * @throws IOException 
   * @throws JsonMappingException 
   * @throws JsonGenerationException 
   */
  public static void main(String[] args) throws ServiceException, JsonGenerationException, JsonMappingException, IOException {
    System.out.println("************************************************************************");
    System.out.println("Generate Test Fixtures for the XRAS Submit Middleware");
    System.out.println("************************************************************************\n");
    
    if (args.length != 1) {
      System.out.println("Usage:");
      System.out.println("    java GenerateFixtures <username>\n");
      System.out.println("For example:");
      System.out.println("    java GenerateFixtures xrasuser\n");
      return;
    }
    
    // output directory
    File outputDir, outputFile;
    outputDir = new File(System.getProperty("user.home") + "/xras_fixtures_" + System.currentTimeMillis());
    outputDir.mkdir();
    System.out.println("Generating test fixtures in directory " + outputDir.getAbsolutePath() + "...");
    
    XrasSession session = new XrasSession(args[0]);
    
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/xras-context.xml");
    
    ObjectMapper mapper = BaseXrasModel.getObjectMapper();
    
    {
      FundingAgencyService fundingAgencyService = (FundingAgencyService) context.getBean("fundingAgencyService");
      // Funding Agencies
      System.out.println("Calling GET /funding_agencies");
      List<FundingAgency> agencies = fundingAgencyService.list(session);
      outputFile = new File(outputDir, "funding_agencies.json");
      mapper.writeValue(outputFile, agencies);
    }
    
    {
      // Opportunities
      OpportunityService opportunityService = (OpportunityService) context.getBean("opportunityService");
      System.out.println("Calling GET /opportunities");
      List<Opportunity> opportunities = opportunityService.list(session);
      outputFile = new File(outputDir, "opportunities.json");
      mapper.writeValue(outputFile, opportunities);
  
      System.out.println("Calling GET /opportunities/" + opportunities.get(0));
      Opportunity opportunity = opportunityService.read(session, new Opportunity(opportunities.get(0).getId()));
      outputFile = new File(outputDir, "opportunity.json");
      mapper.writeValue(outputFile, opportunity);
    }
    
    {
      // Panels
      PanelService panelService = (PanelService) context.getBean("panelService");
      System.out.println("Calling GET /panels");
      List<Panel> panels = panelService.list(session);
      outputFile = new File(outputDir, "panels.json");
      mapper.writeValue(outputFile, panels);
  
      System.out.println("Calling GET /panels/" + panels.get(0));
      Panel panel = panelService.read(session, new Panel(panels.get(0).getId()));
      outputFile = new File(outputDir, "panel.json");
      mapper.writeValue(outputFile, panel);
    }
    
    {
      // People
      PersonService personServce = (PersonService) context.getBean("personService");
      System.out.println("Calling GET /people/<username>");
      Person per = personServce.read(session, new Person(session.getUserContext()));
      outputFile = new File(outputDir, "person.json");
      mapper.writeValue(outputFile, per);
    }
    
    {
      // Requests
      RequestService requestService = (RequestService) context.getBean("requestService");
      RequestReviewService requestReviewService = (RequestReviewService) context.getBean("requestReviewService");
      System.out.println("Calling GET /requests");
      List<Request> requests = requestService.list(session);
      outputFile = new File(outputDir, "requests.json");
      mapper.writeValue(outputFile, requests);
      
      System.out.println("Calling GET /requests/" + requests.get(0));
      Request request = requestService.read(session, new Request(requests.get(0).getId()));
      outputFile = new File(outputDir, "request.json");
      mapper.writeValue(outputFile, request);
      
      System.out.println("Calling GET /requests/" + requests.get(0) + "/reviews");
      RequestReview review = requestReviewService.get(session, new Request(requests.get(0).getId()));
      outputFile = new File(outputDir, "request_reviews.json");
      mapper.writeValue(outputFile, review);
    }
    
    {
      // Resources
      ResourceService resourceService = (ResourceService) context.getBean("resourceService");
      System.out.println("Calling GET /resources");
      List<Resource> resources = resourceService.list(session);
      outputFile = new File(outputDir, "resources.json");
      mapper.writeValue(outputFile, resources);
      
      System.out.println("Calling GET /resources/" + resources.get(0));
      Resource resource = resourceService.read(session, new Resource(resources.get(0).getId()));
      outputFile = new File(outputDir, "resource.json");
      mapper.writeValue(outputFile, resource);
    }
    
    {
      // Types
      List<? extends Type> types;
      
      ActionTypeService actionTypeService = (ActionTypeService) context.getBean("actionTypeService");
      System.out.println("Calling GET /types/actions");
      types = actionTypeService.list(session);
      outputFile = new File(outputDir, "types_actions.json");
      mapper.writeValue(outputFile, types);

      AllocationDateTypeService allocationDateTypeService = (AllocationDateTypeService) context.getBean("allocationDateTypeService");
      System.out.println("Calling GET /types/allocation_dates");
      types = allocationDateTypeService.list(session);
      outputFile = new File(outputDir, "types_allocation_dates.json");
      mapper.writeValue(outputFile, types);

      AttributeSetRelationTypeService attributeSetRelationTypeService = (AttributeSetRelationTypeService) context.getBean("attributeSetRelationTypeService");
      System.out.println("Calling GET /types/attribute_set_relations");
      types = attributeSetRelationTypeService.list(session);
      outputFile = new File(outputDir, "types_attribute_set_relations.json");
      mapper.writeValue(outputFile, types);

      AttributeSetTypeService attributeSetTypeService = (AttributeSetTypeService) context.getBean("attributeSetTypeService");
      System.out.println("Calling GET /types/attribute_sets");
      types = attributeSetTypeService.list(session);
      outputFile = new File(outputDir, "types_attribute_sets.json");
      mapper.writeValue(outputFile, types);

      DocumentTypeService documentTypeService = (DocumentTypeService) context.getBean("documentTypeService");
      System.out.println("Calling GET /types/documents");
      types = documentTypeService.list(session);
      outputFile = new File(outputDir, "types_documents.json");
      mapper.writeValue(outputFile, types);

      FieldOfScienceTypeService fieldOfScienceTypeService = (FieldOfScienceTypeService) context.getBean("fieldOfScienceTypeService");
      System.out.println("Calling GET /types/fos");
      types = fieldOfScienceTypeService.list(session);
      outputFile = new File(outputDir, "types_fos.json");
      mapper.writeValue(outputFile, types);

      OpportunityStateTypeService opportunityStateTypeService = (OpportunityStateTypeService) context.getBean("opportunityStateTypeService");
      System.out.println("Calling GET /types/opportunity_states");
      types = opportunityStateTypeService.list(session);
      outputFile = new File(outputDir, "types_opportunity_states.json");
      mapper.writeValue(outputFile, types);

      PermissionTypeService permissionTypeService = (PermissionTypeService) context.getBean("permissionTypeService");
      System.out.println("Calling GET /types/permissions");
      types = permissionTypeService.list(session);
      outputFile = new File(outputDir, "types_permissions.json");
      mapper.writeValue(outputFile, types);

      RequestStatusTypeService requestStatusTypeService = (RequestStatusTypeService) context.getBean("requestStatusTypeService");
      System.out.println("Calling GET /types/request_status");
      types = requestStatusTypeService.list(session);
      outputFile = new File(outputDir, "types_request_status.json");
      mapper.writeValue(outputFile, types);

      RequestTypeService requestTypeService = (RequestTypeService) context.getBean("requestTypeService");
      System.out.println("Calling GET /types/requests");
      types = requestTypeService.list(session);
      outputFile = new File(outputDir, "types_requests.json");
      mapper.writeValue(outputFile, types);

      ResourceNumberTypeService resourceNumberTypeService = (ResourceNumberTypeService) context.getBean("resourceNumberTypeService");
      System.out.println("Calling GET /types/resource_numbers");
      types = resourceNumberTypeService.list(session);
      outputFile = new File(outputDir, "types_resource_numbers.json");
      mapper.writeValue(outputFile, types);

      ResourceTypeService resourceTypeService = (ResourceTypeService) context.getBean("resourceTypeService");
      System.out.println("Calling GET /types/resources");
      types = resourceTypeService.list(session);
      outputFile = new File(outputDir, "types_resources.json");
      mapper.writeValue(outputFile, types);

      RoleTypeService roleTypeService = (RoleTypeService) context.getBean("roleTypeService");
      System.out.println("Calling GET /types/roles");
      types = roleTypeService.list(session);
      outputFile = new File(outputDir, "types_roles.json");
      mapper.writeValue(outputFile, types);

      UnitTypeService unitTypeService = (UnitTypeService) context.getBean("unitTypeService");
      System.out.println("Calling GET /types/units");
      types = unitTypeService.list(session);
      outputFile = new File(outputDir, "types_units.json");
      mapper.writeValue(outputFile, types);
    }
    
    System.out.println("Done!");

    context.close();
  }

} 