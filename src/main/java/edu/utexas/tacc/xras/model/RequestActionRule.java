/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class RequestActionRule extends BaseModel {

  private int actionId;
  
  private String actionType;
  
  private List<String> allowedOperations;
  
  private boolean reviewsViewable;

  /**
   * @return the actionId
   */
  public int getActionId() {
    return actionId;
  }

  /**
   * @param actionId the actionId to set
   */
  public void setActionId(int actionId) {
    this.actionId = actionId;
  }

  /**
   * @return the actionType
   */
  public String getActionType() {
    return actionType;
  }

  /**
   * @param actionType the actionType to set
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  /**
   * @return the allowedOperations
   */
  public List<String> getAllowedOperations() {
    return allowedOperations;
  }

  /**
   * @param allowedOperations the allowedOperations to set
   */
  public void setAllowedOperations(List<String> allowedOperations) {
    this.allowedOperations = allowedOperations;
  }

  /**
   * @return the reviewsViewable
   */
  public boolean isReviewsViewable() {
    return reviewsViewable;
  }

  /**
   * @param reviewsViewable the reviewsViewable to set
   */
  public void setReviewsViewable(boolean reviewsViewable) {
    this.reviewsViewable = reviewsViewable;
  }
}
