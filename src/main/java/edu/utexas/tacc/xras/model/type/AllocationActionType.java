package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by carrie on 6/2/17.
 *
 * This isn't actually a "type" like all the other types so it's not subclassing Type because fuck consistancy I guess
 */
public class AllocationActionType {

    private int allocationTypeId;
    private List<PageLimitActionType> pageLimitForActionType = new ArrayList<PageLimitActionType>();

    @JsonProperty("allocationTypeId")
    public int getAllocationTypeId() {
        return allocationTypeId;
    }

    public void setAllocationTypeId(int allocationTypeId) {
        this.allocationTypeId = allocationTypeId;
    }

    @JsonProperty("actionTypes")
    public List<PageLimitActionType> getPageLimitForActionType() {
        return pageLimitForActionType;
    }

    public void setPageLimitForActionType(List<PageLimitActionType> pageLimitForActionType) {
        this.pageLimitForActionType = pageLimitForActionType;
    }
}
