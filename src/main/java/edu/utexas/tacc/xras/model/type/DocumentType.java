/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mrhanlon
 * 
 */
public class DocumentType extends Type {

	public DocumentType() {
	}

	public DocumentType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/documents";

	private String documentType;

	private String displayDocumentType;

	private boolean isActive = true;

	private int relativeOrder;

	//private int allocationTypeId;

	private List<AllocationActionType> allocationActionTypes = new ArrayList<>();


	@Override
	@JsonProperty("documentTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the documentType
	 */
	public String getDocumentType() {
		return documentType;
	}

	/**
	 * @param documentType
	 *            the documentType to set
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	/**
	 * @return the displayDocumentType
	 */
	public String getDisplayDocumentType() {
		return displayDocumentType;
	}

	/**
	 * @param displayDocumentType
	 *            the displayDocumentType to set
	 */
	public void setDisplayDocumentType(String displayDocumentType) {
		this.displayDocumentType = displayDocumentType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

	@JsonProperty("allocationActionTypes")
	public List<AllocationActionType> getAllocationActionTypes() {
		return allocationActionTypes;
	}

	public void setAllocationActionTypes(List<AllocationActionType> allocationActionTypes) {
		this.allocationActionTypes = allocationActionTypes;
	}

	@JsonProperty("isActive")
	public boolean getActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	public int getRelativeOrder() {
		return relativeOrder;
	}

	public void setRelativeOrder(int relativeOrder) {
		this.relativeOrder = relativeOrder;
	}
}
