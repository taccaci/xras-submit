/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class ResourceNumberType extends Type {

	public ResourceNumberType() {
	}

	public ResourceNumberType(int id) {
		super(id);
	}

	private String resourceNumberType;

	private String displayResourceNumberType;

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.utexas.tacc.model.XrasModel#getId()
	 */
	@Override
	@JsonProperty("resourceNumberTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the resourceNumberType
	 */
	public String getResourceNumberType() {
		return resourceNumberType;
	}

	/**
	 * @param resourceNumberType
	 *            the resourceNumberType to set
	 */
	public void setResourceNumberType(String resourceNumberType) {
		this.resourceNumberType = resourceNumberType;
	}

	/**
	 * @return the displayResourceNumberType
	 */
	public String getDisplayResourceNumberType() {
		return displayResourceNumberType;
	}

	/**
	 * @param displayResourceNumberType
	 *            the displayResourceNumberType to set
	 */
	public void setDisplayResourceNumberType(String displayResourceNumberType) {
		this.displayResourceNumberType = displayResourceNumberType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.utexas.tacc.model.Type#getTypeUrl()
	 */
	@Override
	public String getTypeUrl() {
		return "/resource_numbers";
	}

}
