/**
 *
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class Panel extends BaseXrasModel<Panel> {

	public Panel() {}
	
	public Panel(int id) {
		super(id);
	}
	
	private String panelName;
	
	private String displayPanelName;
	
	private String panelAbbr;
	
	private String description;
	
	private boolean active;
	
	private String panelType;
	
	private List<String> members;

	public static final String BASE_URL = "/panels";

	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	@Override
	@JsonProperty("panelId")
	public int getId() {
		return id;
	}

	/**
	 * @return the panelName
	 */
	public String getPanelName() {
		return panelName;
	}

	/**
	 * @param panelName the panelName to set
	 */
	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}

	/**
	 * @return the displayPanelName
	 */
	public String getDisplayPanelName() {
		return displayPanelName;
	}

	/**
	 * @param displayPanelName the displayPanelName to set
	 */
	public void setDisplayPanelName(String displayPanelName) {
		this.displayPanelName = displayPanelName;
	}

	/**
	 * @return the panelAbbr
	 */
	public String getPanelAbbr() {
		return panelAbbr;
	}

	/**
	 * @param panelAbbr the panelAbbr to set
	 */
	public void setPanelAbbr(String panelAbbr) {
		this.panelAbbr = panelAbbr;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the active
	 */
	@JsonProperty("isActive")
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the panelType
	 */
	public String getPanelType() {
		return panelType;
	}

	/**
	 * @param panelType the panelType to set
	 */
	public void setPanelType(String panelType) {
		this.panelType = panelType;
	}

	/**
	 * @return the members
	 */
	public List<String> getMembers() {
		return members;
	}

	/**
	 * @param members the members to set
	 */
	public void setMembers(List<String> members) {
		this.members = members;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((panelAbbr == null) ? 0 : panelAbbr.hashCode());
		result = prime * result + ((panelName == null) ? 0 : panelName.hashCode());
		result = prime * result + ((panelType == null) ? 0 : panelType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Panel other = (Panel) obj;
		if (active != other.active)
			return false;
		if (panelAbbr == null) {
			if (other.panelAbbr != null)
				return false;
		} else if (!panelAbbr.equals(other.panelAbbr))
			return false;
		if (panelName == null) {
			if (other.panelName != null)
				return false;
		} else if (!panelName.equals(other.panelName))
			return false;
		if (panelType == null) {
			if (other.panelType != null)
				return false;
		} else if (!panelType.equals(other.panelType))
			return false;
		return true;
	}

}
