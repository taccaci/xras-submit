/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class PermissionType extends Type {

	public PermissionType() {
	}

	public PermissionType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/permissions";

	private String permissionType;

	private String displayPermissionType;

	@Override
	@JsonProperty("permissionTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the permissionType
	 */
	public String getPermissionType() {
		return permissionType;
	}

	/**
	 * @param permissionType
	 *            the permissionType to set
	 */
	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}

	/**
	 * @return the displayPermissionType
	 */
	public String getDisplayPermissionType() {
		return displayPermissionType;
	}

	/**
	 * @param displayPermissionType
	 *            the displayPermissionType to set
	 */
	public void setDisplayPermissionType(String displayPermissionType) {
		this.displayPermissionType = displayPermissionType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
