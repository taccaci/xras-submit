/**
 * 
 */
package edu.utexas.tacc.xras.model.mixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author mrhanlon
 *
 */
@JsonInclude(Include.NON_EMPTY)
public class NotEmptyMixin {

}
