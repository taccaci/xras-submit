/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class RequestConflict extends BaseXrasModel<RequestConflict> {

	private String conflictType;
	private String conflictPerson;

	/**
	 * @return the id
	 */
	@JsonProperty("conflictId")
	public int getId() {
		return id;
	}

	/**
	 * @return the conflictType
	 */
	public String getConflictType() {
		return conflictType;
	}

	/**
	 * @param conflictType the conflictType to set
	 */
	public void setConflictType(String conflictType) {
		this.conflictType = conflictType;
	}

	/**
	 * @return the conflictPerson
	 */
	public String getConflictPerson() {
		return conflictPerson;
	}

	/**
	 * @param conflictPerson the conflictPerson to set
	 */
	public void setConflictPerson(String conflictPerson) {
		this.conflictPerson = conflictPerson;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/conflicts";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conflictPerson == null) ? 0 : conflictPerson.hashCode());
		result = prime * result + ((conflictType == null) ? 0 : conflictType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestConflict other = (RequestConflict) obj;
		if (conflictPerson == null) {
			if (other.conflictPerson != null) {
				return false;
			}
		} else if (!conflictPerson.equals(other.conflictPerson)) {
			return false;
		}
		if (conflictType == null) {
			if (other.conflictType != null) {
				return false;
			}
		} else if (!conflictType.equals(other.conflictType)) {
			return false;
		}
		return true;
	}

}