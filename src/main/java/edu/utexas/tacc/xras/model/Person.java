/**
 *
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties(value = {"id"}, ignoreUnknown = true)
public class Person extends BaseXrasModel<Person> {

	public static final String BASE_URL = "/people";

	public Person() {}

	public Person(String username) {
		setUsername(username);
	}
	
	@NotNull
	private String username;
	
	@NotNull
	private String firstName;
	
	private String middleName;
	
	@NotNull
	private String lastName;
	
	@NotNull @Email
	private String email;
	
	private String phone;
	
	private String position;
	
	private String country;
	
	private String organization;
	
	private String academicStatus;
	
	private List<AcademicDegree> academicDegrees = new ArrayList<AcademicDegree>();
	
	private List<String> citizenship = new ArrayList<String>();

	private String residenceCountry;

	private Boolean reconciled;

	@Override
	public int getId() {
		return id;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return the academicStatus
	 */
	public String getAcademicStatus() {
		return academicStatus;
	}

	/**
	 * @param academicStatus the academicStatus to set
	 */
	public void setAcademicStatus(String academicStatus) {
		this.academicStatus = academicStatus;
	}

	/**
	 * @return the academicDegrees
	 */
	public List<AcademicDegree> getAcademicDegrees() {
		return academicDegrees;
	}

	/**
	 * @param academicDegrees the academicDegrees to set
	 */
	public void setAcademicDegrees(List<AcademicDegree> academicDegrees) {
		this.academicDegrees = academicDegrees;
	}

	/**
	 * @return the citizenship
	 */
	public List<String> getCitizenship() {
		return citizenship;
	}

	/**
	 * @param citizenship the citizenship to set
	 */
	public void setCitizenship(List<String> citizenship) {
		this.citizenship = citizenship;
	}

	public String getResidenceCountry() {
		return residenceCountry;
	}

	public void setResidenceCountry(String residenceCountry) {
		this.residenceCountry = residenceCountry;
	}

	@JsonProperty("isReconciled")
	public Boolean getReconciled() {
		return reconciled;
	}

	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}

	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}
	
	@Override
	public String getInstanceUrl() {
		return this.getBaseUrl() + "/" + this.getUsername();
	}

	@Override
	public boolean isNew() {
		return this.getUsername() == null || this.getUsername().length() == 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
