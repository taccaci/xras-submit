package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;
import org.joda.time.DateTime;

public class Announcement extends BaseXrasModel<Announcement> {

    private DateTime beginDate;
    private DateTime endDate;
    private String title;
    private String message;
    private String severityLevel;

    @JsonSerialize(using = JodaDateOnlySerializer.class)
    public DateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(DateTime beginDate) {
        this.beginDate = beginDate;
    }

    @JsonSerialize(using = JodaDateOnlySerializer.class)
    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSeverityLevel() {
        return severityLevel;
    }

    public void setSeverityLevel(String severityLevel) {
        this.severityLevel = severityLevel;
    }

    @Override
    public boolean equals(Object object) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public int getId() {
        return 0;
    }

    public static final String BASE_URL = "/announcements";

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
