/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author mrhanlon
 *
 */
public class ResourceAttributeValue extends BaseXrasModel<ResourceAttributeValue> {

	private String attributeValue;
	
	private int resourceId;

	/**
	 * @return the resourceAttributeId
	 */
	@JsonProperty("resourceAttributeId")
	public int getId() {
		return id;
	}
  
  public int getAttributeId() {
    return id;
  }

	/**
	 * @return the attributeValue
	 */
	public String getAttributeValue() {
		return attributeValue;
	}

	/**
	 * @param attributeValue the attributeValue to set
	 */
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	/**
   * @return the resourceId
   */
  public int getResourceId() {
    return resourceId;
  }

  /**
   * @param resourceId the resourceId to set
   */
  public void setResourceId(int resourceId) {
    this.resourceId = resourceId;
  }

  /* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/resource_attributes";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ResourceAttributeValue other = (ResourceAttributeValue) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
