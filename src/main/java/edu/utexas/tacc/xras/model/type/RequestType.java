/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestType extends Type {

	public RequestType() {
	}

	public RequestType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/requests";

	private String requestType;

	private String displayRequestType;

	private boolean active;

	@Override
	@JsonProperty("requestTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType
	 *            the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the displayRequestType
	 */
	public String getDisplayRequestType() {
		return displayRequestType;
	}

	/**
	 * @param displayRequestType
	 *            the displayRequestType to set
	 */
	public void setDisplayRequestType(String displayRequestType) {
		this.displayRequestType = displayRequestType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

	@JsonProperty("isActive")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
