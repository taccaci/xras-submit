/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class RequestFos extends BaseXrasModel<RequestFos> {

  public RequestFos() {}
  
  public RequestFos(int id) {
    this.setId(id);
  }
  
	private boolean primary;
	private int fosNum;

	/**
	 * @return the fosTypeId
	 */
	@JsonProperty("fosTypeId")
	public int getId() {
		return id;
	}
	
	/**
	 * @return the fosNum
	 */
	public int getFosNum() {
		return fosNum;
	}

	/**
	 * @param fosNum the fosNum to set
	 */
	public void setFosNum(int fosNum) {
		this.fosNum = fosNum;
	}

	/**
	 * @return the primary
	 */
	@JsonProperty("isPrimary")
	public boolean isPrimary() {
		return primary;
	}

	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/fos";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestFos other = (RequestFos) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}
}
