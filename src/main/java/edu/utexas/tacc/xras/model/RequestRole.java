/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestRole extends BaseXrasModel<RequestRole> {
	
	private String role;
	
	private DateTime beginDate;
	
	private DateTime endDate;
	
	private boolean accountToBeCreated = true;

	private String roleTypeId;

	private boolean isActive = true;

	public String getRoleTypeId() {
		return roleTypeId;
	}

	public void setRoleTypeId(String roleTypeId) {
		this.roleTypeId = roleTypeId;
	}

	/**
	 * @return the id
	 */
	@JsonProperty("roleId")
	public int getId() {
		return id;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the beginDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(DateTime beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	/**
   * @return the accountToBeCreated
   */
	@JsonProperty("isAccountToBeCreated")
  public boolean isAccountToBeCreated() {
    return accountToBeCreated;
  }

  /**
   * @param accountToBeCreated the accountToBeCreated to set
   */
  public void setAccountToBeCreated(boolean accountToBeCreated) {
    this.accountToBeCreated = accountToBeCreated;
  }

  /*
   * a role for a user is active if:
   * - the endDate is null
   * - the endDate is after now
   */

  public boolean isActive() {
  	return (endDate == null || endDate.isAfterNow());
  }

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/roles";
	}

	/* (non-Javadoc)
   * @see edu.utexas.tacc.xras.model.BaseXrasModel#getInstanceUrl()
   */
  @Override
  public String getInstanceUrl() {
    if (isNew()) {
      return this.getBaseUrl() + "/" + this.getRole();
    } else {
      return super.getInstanceUrl();
    }
  }

  public ObjectNode toJSON() {
		ObjectNode json = getObjectMapper().createObjectNode();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
		if (getBeginDate() != null) {
		  json.put("beginDate", dtf.print(getBeginDate()));
		}
		if (getEndDate() != null) {
		  json.put("endDate", dtf.print(getEndDate()));
		}
		json.put("roleType", getRole());
		json.put("isAccountToBeCreated", isAccountToBeCreated());
		return json;
	}

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    RequestRole other = (RequestRole) obj;
    if (id != other.id) {
      return false;
    } else if (isNew()) {
      if (role == null) {
        if (other.role != null) {
          return false;
        }
      } else if (!role.equals(other.role)) {
        return false;
      }
    }
    return true;
  }

}
