/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import edu.utexas.tacc.xras.model.serializer.BigDecimalDeserializer;
import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class OpportunityResourceNumber extends BaseModel {
	
	private String source;

	private BigDecimal amount;
	
	private DateTime beginDate;
	
	private DateTime endDate;
	
	private int resourceNumberTypeId;
	
	private String displayResourceNumberType;

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the amount
	 */
	@JsonSerialize(using=ToStringSerializer.class)
  @JsonDeserialize(using=BigDecimalDeserializer.class)
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the beginDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(DateTime beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the resourceNumberTypeId
	 */
	public int getResourceNumberTypeId() {
		return resourceNumberTypeId;
	}

	/**
	 * @param resourceNumberTypeId the resourceNumberTypeId to set
	 */
	public void setResourceNumberTypeId(int resourceNumberTypeId) {
		this.resourceNumberTypeId = resourceNumberTypeId;
	}

	/**
	 * @return the displayResourceNumberType
	 */
	public String getDisplayResourceNumberType() {
		return displayResourceNumberType;
	}

	/**
	 * @param displayResourceNumberType the displayResourceNumberType to set
	 */
	public void setDisplayResourceNumberType(String displayResourceNumberType) {
		this.displayResourceNumberType = displayResourceNumberType;
	}
}
