/**
 *
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request extends BaseXrasModel<Request> {

	public Request() {}
	
	public Request(int id) {
		super(id);
	}

	public static final String BASE_URL = "/requests";
	
	private RequestRule rules;
	
	private String allocationsProcess;
	
	private String requestNumber;
	
	private String requestStatus;
	
	private String requestType;
	
	private int opportunityId;
	
	private String title;
	
	private String abstractText;
	
	private Date submitDate;

	private Date beginDate;

	private Date endDate;
	
	private boolean deleted;
	
	private Boolean supportedByGrants;
	
	private List<RequestAction> actions = new ArrayList<RequestAction>();
	
	private List<String> requestStates = new ArrayList<String>();
	
	private List<RequestPersonRole> roles = new ArrayList<RequestPersonRole>();
	
	private List<RequestFos> fos = new ArrayList<RequestFos>();
	
	private String keywords;
	
	private List<RequestGrant> grants = new ArrayList<RequestGrant>();
	
	private List<RequestConflict> conflicts = new ArrayList<RequestConflict>();
	
	private List<RequestPublication> publications = new ArrayList<RequestPublication>();

	private String shortTitle;

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	@Override
	@JsonProperty("requestId")
	public int getId() {
		return id;
	}

	/**
   * @return the rules
   */
  public RequestRule getRules() {
    return rules;
  }

  /**
   * @param rules the rules to set
   */
  public void setRules(RequestRule rules) {
    this.rules = rules;
  }

  public String getAllocationsProcess() {
		return allocationsProcess;
	}

	public void setAllocationsProcess(String allocationsProcess) {
		this.allocationsProcess = allocationsProcess;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getRequestType() {
		return this.requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the opportunityId
	 */
	public int getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(int opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the abstractText
	 */
	@JsonProperty("abstract")
	public String getAbstractText() {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	/**
	 * @return the submitDate
	 */
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	public Date getSubmitDate() {
		return submitDate;
	}

	/**
	 * @param submitDate the submitDate to set
	 */
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	/**
	 * @return the beginDate
	 */
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	public Date getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the deleted
	 */
	@JsonProperty("isDeleted")
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the supportedByGrants
	 */
	@JsonProperty("isSupportedByGrants")
	public Boolean isSupportedByGrants() {
		return supportedByGrants;
	}

	/**
	 * @param supportedByGrants the supportedByGrants to set
	 */
	public void setSupportedByGrants(Boolean supportedByGrants) {
		this.supportedByGrants = supportedByGrants;
	}
	/**
	 * @return the actions
	 */
	public List<RequestAction> getActions() {
		return actions;
	}

	/**
	 * @param actions the actions to set
	 */
	public void setActions(List<RequestAction> actions) {
		this.actions = actions;
	}

	/**
	 * @return the requestStates
	 */
	public List<String> getRequestStates() {
		return requestStates;
	}

	/**
	 * @param requestStates the requestStates to set
	 */
	public void setRequestStates(List<String> requestStates) {
		this.requestStates = requestStates;
	}

	/**
	 * @return the roles
	 */
	public List<RequestPersonRole> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<RequestPersonRole> roles) {
		this.roles = roles;
	}

	/**
	 * @return the fos
	 */
	public List<RequestFos> getFos() {
		return fos;
	}

	/**
	 * @param fos the fos to set
	 */
	public void setFos(List<RequestFos> fos) {
		this.fos = fos;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the grants
	 */
	public List<RequestGrant> getGrants() {
		return grants;
	}

	/**
	 * @param grants the grants to set
	 */
	public void setGrants(List<RequestGrant> grants) {
		this.grants = grants;
	}

	/**
	 * @return the conflicts
	 */
	public List<RequestConflict> getConflicts() {
		return conflicts;
	}

	/**
	 * @param conflicts the conflicts to set
	 */
	public void setConflicts(List<RequestConflict> conflicts) {
		this.conflicts = conflicts;
	}

	/**
	 * @return the publications
	 */
	public List<RequestPublication> getPublications() {
		return publications;
	}

	/**
	 * @param publications the publications to set
	 */
	public void setPublications(List<RequestPublication> publications) {
		this.publications = publications;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + opportunityId;
		result = prime * result + ((requestNumber == null) ? 0 : requestNumber.hashCode());
		result = prime * result + ((requestStatus == null) ? 0 : requestStatus.hashCode());
		result = prime * result + ((requestType == null) ? 0 : requestType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (opportunityId != other.opportunityId)
			return false;
		if (requestNumber == null) {
			if (other.requestNumber != null)
				return false;
		} else if (!requestNumber.equals(other.requestNumber))
			return false;
		if (requestStatus == null) {
			if (other.requestStatus != null)
				return false;
		} else if (!requestStatus.equals(other.requestStatus))
			return false;
		if (requestType == null) {
			if (other.requestType != null)
				return false;
		} else if (!requestType.equals(other.requestType))
			return false;
		return true;
	}

}
