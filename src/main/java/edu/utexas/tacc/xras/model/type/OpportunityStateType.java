/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class OpportunityStateType extends Type {

	public OpportunityStateType() {
	}

	public OpportunityStateType(int id) {
		super(id);
	}

	private String opportunityStateType;

	private String displayOpportunityStateType;

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.utexas.tacc.model.XrasModel#getId()
	 */
	@Override
	@JsonProperty("opportunityStateTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the opportunityStateType
	 */
	public String getOpportunityStateType() {
		return opportunityStateType;
	}

	/**
	 * @param opportunityStateType
	 *            the opportunityStateType to set
	 */
	public void setOpportunityStateType(String opportunityStateType) {
		this.opportunityStateType = opportunityStateType;
	}

	/**
	 * @return the displayOpportunityStateType
	 */
	public String getDisplayOpportunityStateType() {
		return displayOpportunityStateType;
	}

	/**
	 * @param displayOpportunityStateType
	 *            the displayOpportunityStateType to set
	 */
	public void setDisplayOpportunityStateType(String displayOpportunityStateType) {
		this.displayOpportunityStateType = displayOpportunityStateType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.utexas.tacc.model.Type#getTypeUrl()
	 */
	@Override
	public String getTypeUrl() {
		return "/opportunity_states";
	}

}
