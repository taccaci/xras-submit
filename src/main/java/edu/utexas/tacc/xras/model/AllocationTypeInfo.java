package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by carrie on 6/13/17.
 */
public class AllocationTypeInfo {

    private int allocationTypeId;
    private String allocationType;
    private String description;
    private Integer numSubmissions;
    private boolean active;
    private Integer relativeOrder;

    public int getAllocationTypeId() {
        return allocationTypeId;
    }

    public void setAllocationTypeId(int allocationTypeId) {
        this.allocationTypeId = allocationTypeId;
    }

    public String getAllocationType() {
        return allocationType;
    }

    public void setAllocationType(String allocationType) {
        this.allocationType = allocationType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumSubmissions() {
        return numSubmissions;
    }

    public void setNumSubmissions(Integer numSubmissions) {
        this.numSubmissions = numSubmissions;
    }

    @JsonProperty("isActive")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        active = active;
    }

    public Integer getRelativeOrder() {
        return relativeOrder;
    }

    public void setRelativeOrder(Integer relativeOrder) {
        this.relativeOrder = relativeOrder;
    }
}
