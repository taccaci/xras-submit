/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class FieldOfScienceType extends Type {

	public FieldOfScienceType() {
	}

	public FieldOfScienceType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/fos";

	private String fosName;

	private String fosAbbr;

	private String fosNum;

	private Integer fosTypeParentId;

	private boolean active;

	private boolean selectable = true;

	/**
	 * @return the fosName
	 */
	public String getFosName() {
		return fosName;
	}

	/**
	 * @param fosName
	 *            the fosName to set
	 */
	public void setFosName(String fosName) {
		this.fosName = fosName;
	}

	/**
	 * @return the fosAbbr
	 */
	public String getFosAbbr() {
		return fosAbbr;
	}

	/**
	 * @param fosAbbr
	 *            the fosAbbr to set
	 */
	public void setFosAbbr(String fosAbbr) {
		this.fosAbbr = fosAbbr;
	}

	/**
	 * @return the fosNum
	 */
	public String getFosNum() {
		return fosNum;
	}

	/**
	 * @param fosNum
	 *            the fosNum to set
	 */
	public void setFosNum(String fosNum) {
		this.fosNum = fosNum;
	}

	/**
	 * @return the fosTypeParentId
	 */
	public Integer getFosTypeParentId() {
		return fosTypeParentId;
	}

	/**
	 * @param fosTypeParentId
	 *            the fosTypeParentId to set
	 */
	public void setFosTypeParentId(Integer fosTypeParentId) {
		this.fosTypeParentId = fosTypeParentId;
	}

	/**
	 * @return the active
	 */
	@JsonProperty("isActive")
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonProperty("isSelectable")
	public boolean isSelectable() { return selectable; }

	public void setSelectable(boolean selectable) { this.selectable = selectable; }

	@Override
	@JsonProperty("fosTypeId")
	public int getId() {
		return id;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
