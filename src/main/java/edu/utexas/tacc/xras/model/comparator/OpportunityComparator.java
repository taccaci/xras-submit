/**
 * 
 */
package edu.utexas.tacc.xras.model.comparator;

import java.util.Comparator;

import org.joda.time.DateTime;

import edu.utexas.tacc.xras.model.Opportunity;

/**
 * @author mrhanlon
 * 
 */
public class OpportunityComparator implements Comparator<Opportunity> {

	/*
	 * (non-Javadoc)
	 *
	 *
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Opportunity opp0, Opportunity opp1) {
		DateTime d0 = opp0.getSubmissionBeginDate(), d1 = opp1.getSubmissionBeginDate();

		if (d0 == null) {
			return 1;
		} else if (d1 == null) {
			return -1;
		} else {
			// reverse chronological
			return d1.compareTo(d0);
		}
	}

}
