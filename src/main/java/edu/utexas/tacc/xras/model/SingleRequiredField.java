package edu.utexas.tacc.xras.model;

/**
 * CURRENTLY UNSUED, may come into play later.
 *
 */

public class SingleRequiredField {

    private boolean isRequired;
    //private String fieldName;
    private String errorMessage;

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
