/**
 * 
 */
package edu.utexas.tacc.xras.model;

/**
 * @author mrhanlon
 *
 */
public class PublicationAuthor extends BaseModel {
	
	private int id;
	
	private int order;
	
	private PublicationPerson author;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * @return the author
	 */
	public PublicationPerson getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(PublicationPerson author) {
		this.author = author;
	}

}
