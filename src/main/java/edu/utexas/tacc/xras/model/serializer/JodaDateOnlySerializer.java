/**
 * 
 */
package edu.utexas.tacc.xras.model.serializer;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * @author mrhanlon
 *
 */
public class JodaDateOnlySerializer extends JsonSerializer<DateTime> {

	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
	
	@Override
	public void serialize(DateTime value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeString(formatter.print(value));
	}

}
