/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import edu.utexas.tacc.xras.model.serializer.BigDecimalDeserializer;
import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class RequestGrant extends BaseXrasModel<RequestGrant> {

	private int fundingAgencyId;
	
	private String grantNumber;
	
	private String piName;
	
	private String title;
	
	private DateTime beginDate;
	
	private DateTime endDate;
	
	private BigDecimal awardedAmount;
	
	private String awardedUnits;
	
	private BigDecimal percentageAward;
	
	private String programOfficerName;
	
	private String programOfficerEmail;
	
	private Boolean pending;
	
	private String subAwardNumber;
	
	private Integer primaryFosTypeId;
	
	private String comments;

	/**
	 * @return the id
	 */
	@JsonProperty("grantId")
	public int getId() {
		return id;
	}

	/**
	 * @return the fundingAgencyId
	 */
	public int getFundingAgencyId() {
		return fundingAgencyId;
	}

	/**
	 * @param fundingAgencyId the fundingAgencyId to set
	 */
	public void setFundingAgencyId(int fundingAgencyId) {
		this.fundingAgencyId = fundingAgencyId;
	}

	/**
	 * @return the grantNumber
	 */
	public String getGrantNumber() {
		return grantNumber;
	}

	/**
	 * @param grantNumber the grantNumber to set
	 */
	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	/**
	 * @return the piName
	 */
	public String getPiName() {
		return piName;
	}

	/**
	 * @param piName the piName to set
	 */
	public void setPiName(String piName) {
		this.piName = piName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the beginDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(DateTime beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the awardedAmount
	 */
	@JsonSerialize(using=ToStringSerializer.class)
  @JsonDeserialize(using=BigDecimalDeserializer.class)
	public BigDecimal getAwardedAmount() {
		return awardedAmount;
	}

	/**
	 * @param awardedAmount the awardedAmount to set
	 */
	public void setAwardedAmount(BigDecimal awardedAmount) {
		this.awardedAmount = awardedAmount;
	}

	/**
	 * @return the awardedUnits
	 */
	public String getAwardedUnits() {
		return awardedUnits;
	}

	/**
	 * @param awardedUnits the awardedUnits to set
	 */
	public void setAwardedUnits(String awardedUnits) {
		this.awardedUnits = awardedUnits;
	}

	/**
	 * @return the percentageAward
	 */
	@JsonSerialize(using=ToStringSerializer.class)
	@JsonDeserialize(using=BigDecimalDeserializer.class)
	public BigDecimal getPercentageAward() {
		return percentageAward;
	}

	/**
	 * @param percentageAward the percentageAward to set
	 */
	public void setPercentageAward(BigDecimal percentageAward) {
		this.percentageAward = percentageAward;
	}

	/**
	 * @return the programOfficerName
	 */
	public String getProgramOfficerName() {
		return programOfficerName;
	}

	/**
	 * @param programOfficerName the programOfficer to set
	 */
	public void setProgramOfficerName(String programOfficerName) {
		this.programOfficerName = programOfficerName;
	}

	/**
	 * @return the programOfficerEmail
	 */
	public String getProgramOfficerEmail() {
		return programOfficerEmail;
	}

	/**
	 * @param programOfficerEmail the programOfficerEmail to set
	 */
	public void setProgramOfficerEmail(String programOfficerEmail) {
		this.programOfficerEmail = programOfficerEmail;
	}

	/**
	 * @return the pending
	 */
	@JsonProperty("isPending")
	public Boolean isPending() {
		return pending;
	}

	/**
	 * @param pending the pending to set
	 */
	public void setPending(Boolean pending) {
		this.pending = pending;
	}

	/**
	 * @return the subAwardNumber
	 */
	public String getSubAwardNumber() {
		return subAwardNumber;
	}

	/**
	 * @param subAwardNumber the subAwardNumber to set
	 */
	public void setSubAwardNumber(String subAwardNumber) {
		this.subAwardNumber = subAwardNumber;
	}

	/**
	 * @return the primaryFosTypeId
	 */
	public Integer getPrimaryFosTypeId() {
		return primaryFosTypeId;
	}

	/**
	 * @param primaryFosTypeId the primaryFosTypeId to set
	 */
	public void setPrimaryFosTypeId(Integer primaryFosTypeId) {
		this.primaryFosTypeId = primaryFosTypeId;
	}

	/**
   * @return the comments
   */
  public String getComments() {
    return comments;
  }

  /**
   * @param comments the comments to set
   */
  public void setComments(String comments) {
    this.comments = comments;
  }

  /* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/grants";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((beginDate == null) ? 0 : beginDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + fundingAgencyId;
		result = prime * result + ((grantNumber == null) ? 0 : grantNumber.hashCode());
		result = prime * result + ((subAwardNumber == null) ? 0 : subAwardNumber.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestGrant other = (RequestGrant) obj;
		if (this.isNew()) {
		  if (! other.isNew()) {
		    return false;
		  } else {
		    if (this.title == null) {
		      if (other.title != null) {
		        return false;
		      }
		    } else {
		      if (! this.title.equals(other.title)) {
		        return false;
		      }
		    }
		    if (this.grantNumber == null) {
		      if (other.title != null) {
            return false;
          }
		    } else {
		      if (! this.grantNumber.equals(other.grantNumber)) {
		        return false;
		      }
		    }
		  }
		} else {
		  if (this.getId() != other.getId()) {
		    return false;
		  }
		}
		return true;
	}
}
