package edu.utexas.tacc.xras.model.parser;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.type.CollectionType;

import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.model.BaseXrasModel;

/**
 * @author mrhanlon
 *
 */
public class XrasParser {

	/**
	 * Parses a model instance from the JSON representation
	 * @param node The model JSON, for example, the result of an {@link ApiResponse}.
	 * @param type The expected <code>class</code> of the response
	 * @return 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T parseModel(JsonNode node, Class<T> type) throws JsonProcessingException, IOException {
		ObjectReader reader = BaseXrasModel.getObjectMapper().reader(type);
		return (T) reader.readValue(node);
	}

//	/**
//	 * Parses a model instance from the JSON representation
//	 * @param node The model JSON, for example, the result of an {@link ApiResponse}.
//	 * @param type The expected <code>class</code> of the response
//	 * @return 
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> T parseModel(JsonNode node, XrasModel<? super T> model) throws JsonProcessingException, IOException {
//		ObjectReader reader = BaseXrasModel.getObjectMapper().readerForUpdating(model);
//		return (T) reader.readValue(node);
//	}

	/**
	 * Parses a List of model instances from the JSON representation
	 * @param <T>
	 * @param node The list JSON, for example, the result of an {@link ApiResponse}. 
	 * @param type The expected <code>class</code> of the response
	 * @return
	 */
	public static <T> List<T> parseList(JsonNode node, Class<T> type) throws JsonProcessingException, IOException {
		CollectionType listType = BaseXrasModel.getObjectMapper().getTypeFactory().constructCollectionType(List.class, type);
		ObjectReader reader = BaseXrasModel.getObjectMapper().reader(listType);
		return reader.readValue(node);
	}
}
