/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class RequestResourceRule extends BaseModel {

  private String actionType;
  
  private List<Integer> availableResourceIds;

  /**
   * @return the actionType
   */
  public String getActionType() {
    return actionType;
  }

  /**
   * @param actionType the actionType to set
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  /**
   * @return the availableResourceIds
   */
  public List<Integer> getAvailableResourceIds() {
    return availableResourceIds;
  }

  /**
   * @param availableResourceIds the availableResourceIds to set
   */
  public void setAvailableResourceIds(List<Integer> availableResourceIds) {
    this.availableResourceIds = availableResourceIds;
  }
}
