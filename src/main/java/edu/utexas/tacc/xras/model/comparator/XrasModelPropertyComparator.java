/**
 * 
 */
package edu.utexas.tacc.xras.model.comparator;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import edu.utexas.tacc.xras.model.XrasModel;

/**
 * @author mrhanlon
 *
 */
public class XrasModelPropertyComparator<T extends XrasModel<? super T>> implements Comparator<XrasModel<T>> {

	private static final Logger logger = Logger.getLogger(XrasModelPropertyComparator.class);
	
	public XrasModelPropertyComparator(String property) {
		this(property, true);
	}
	
	public XrasModelPropertyComparator(String property, boolean ascending) {
		this.property = property;
		this.ascending = ascending;
	}
	
	private String property;
	
	private boolean ascending;
	
	/**
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param property the property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * @return the ascending
	 */
	public boolean isAscending() {
		return ascending;
	}

	/**
	 * @param ascending the ascending to set
	 */
	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(XrasModel<T> model0, XrasModel<T> model1) {
		try {
			Object o0 = PropertyUtils.getProperty(model0, property),
				   o1 = PropertyUtils.getProperty(model1, property);
			
			if (o0 == null || o1 == null) {
				return o0 == null ? 1 : -1;
			} else {
				BeanComparator bc = new BeanComparator(property);
				if (ascending) {
					return bc.compare(model0, model1);
				} else {
					return bc.compare(model1, model0);
				}
			}
		} catch (NoSuchMethodException e) {
			logger.error("Error comparing objects", e);
		} catch (IllegalAccessException e) {
			logger.error("Error comparing objects", e);
		} catch (InvocationTargetException e) {
			logger.error("Error comparing objects", e);
		}
		return 0;
	}

}
