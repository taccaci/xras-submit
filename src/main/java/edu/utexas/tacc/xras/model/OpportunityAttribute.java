/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class OpportunityAttribute extends BaseModel {

	private int id;
	
	private String attributeName;

	private String description;
	private Boolean required;
	private String relativeOrder;

	/**
	 * @return the id
	 */
	@JsonProperty("opportunityAttributeId")
	public int getId() {
		return id;
	}
  
  public int getAttributeId() {
    return id;
  }

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@JsonProperty("isRequired")
	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getRelativeOrder() {
		return relativeOrder;
	}

	public void setRelativeOrder(String relativeOrder) {
		this.relativeOrder = relativeOrder;
	}
}
