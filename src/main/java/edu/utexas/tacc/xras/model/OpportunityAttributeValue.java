/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 * 
 */
public class OpportunityAttributeValue extends BaseXrasModel<OpportunityAttributeValue> {

	private String attributeValue;

	/**
	 * @return the opportunityAttributeId
	 */
	@JsonProperty("opportunityAttributeId")
	public int getId() {
		return id;
	}
	
	public int getAttributeId() {
	  return id;
	}

	/**
	 * @return the attributeValue
	 */
	public String getAttributeValue() {
		return attributeValue;
	}

	/**
	 * @param attributeValue
	 *            the attributeValue to set
	 */
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/opportunity_attributes";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributeValue == null) ? 0 : attributeValue.hashCode());
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OpportunityAttributeValue other = (OpportunityAttributeValue) obj;
		if (attributeValue == null) {
			if (other.attributeValue != null) {
				return false;
			}
		} else if (!attributeValue.equals(other.attributeValue)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
