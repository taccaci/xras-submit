/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class AttributeSet<T> extends BaseModel {
	
	private int id;
	
	private int attributeSetTypeId;
	
	private int attributeSetRelationTypeId;
	
	private String attributeSetName;
	
	private List<T> attributes;

	private Integer relativeOrder;

	@JsonProperty("isActive")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	private boolean active;

	public Integer getRelativeOrder() {
		return relativeOrder;
	}

	public void setRelativeOrder(Integer relativeOrder) {
		this.relativeOrder = relativeOrder;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	@JsonProperty("attributeSetId")
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the attributeSetTypeId
	 */
	public int getAttributeSetTypeId() {
		return attributeSetTypeId;
	}

	/**
	 * @param attributeSetTypeId the attributeSetTypeId to set
	 */
	public void setAttributeSetTypeId(int attributeSetTypeId) {
		this.attributeSetTypeId = attributeSetTypeId;
	}

	/**
	 * @return the attributeSetRelationId
	 */
	public int getAttributeSetRelationTypeId() {
		return attributeSetRelationTypeId;
	}

	/**
	 * @param attributeSetRelationTypeId the attributeSetRelationTypeId to set
	 */
	public void setAttributeSetRelationTypeId(int attributeSetRelationTypeId) {
		this.attributeSetRelationTypeId = attributeSetRelationTypeId;
	}

	/**
	 * @return the attributeSetName
	 */
	public String getAttributeSetName() {
		return attributeSetName;
	}

	/**
	 * @param attributeSetName the attributeSetName to set
	 */
	public void setAttributeSetName(String attributeSetName) {
		this.attributeSetName = attributeSetName;
	}

	/**
	 * @return the attributes
	 */
	public List<T> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(List<T> attributes) {
		this.attributes = attributes;
	}

}
