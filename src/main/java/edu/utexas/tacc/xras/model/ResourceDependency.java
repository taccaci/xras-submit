/**
 * 
 */
package edu.utexas.tacc.xras.model;

import org.joda.time.DateTime;

/**
 * @author mrhanlon
 *
 */
public class ResourceDependency extends BaseModel {

	private int resourceId;
	
	private String resourceName;
	
	private String displayResourceName;
	
	private DateTime beginDate;
	
	private DateTime endDate;

	/**
	 * @return the resourceId
	 */
	public int getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the displayResourceName
	 */
	public String getDisplayResourceName() {
		return displayResourceName;
	}

	/**
	 * @param displayResourceName the displayResourceName to set
	 */
	public void setDisplayResourceName(String displayResourceName) {
		this.displayResourceName = displayResourceName;
	}

	/**
	 * @return the beginDate
	 */
	public DateTime getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(DateTime beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}
}
