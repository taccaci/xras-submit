/**
 *
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class Opportunity extends BaseXrasModel<Opportunity> {

	public Opportunity() {}
	
	public Opportunity(int id) {
		super(id);
	}
	
	private OpportunityRule rules;
	
	private String opportunityName;
	
	private String displayOpportunityName;
	
	private String opportunityType;
	
	private DateTime announcementDate;
	
	private DateTime submissionBeginDate;
	
	private DateTime submissionEndDate;
	
	private String submissionURL;
	
	private int defaultAllocationAwardPeriod;
	
	private String allocationType;

	private AllocationTypeInfo allocationTypeInfo;
	
	private List<OpportunityResource> resources = new ArrayList<OpportunityResource>();
	
	private List<OpportunityPanel> panels = new ArrayList<OpportunityPanel> ();
	
	private List<String> opportunityStates = new ArrayList<String>();
	
	private List<AttributeSet<OpportunityAttribute>> attributeSets = new ArrayList<AttributeSet<OpportunityAttribute>>();
	
	private OpportunityTermination terminatingOpportunity;
	
	private String adminComments;

  private String submissionNotes;

  private int relativeOrder;

	@Override
	@JsonProperty("opportunityId")
	public int getId() {
		return id;
	}

	/**
   * @return the rules
   */
  public OpportunityRule getRules() {
    return rules;
  }

  /**
   * @param rules the rules to set
   */
  public void setRules(OpportunityRule rules) {
    this.rules = rules;
  }

  /**
	 * @return the opportunityName
	 */
	public String getOpportunityName() {
		return opportunityName;
	}

	/**
	 * @param opportunityName the opportunityName to set
	 */
	public void setOpportunityName(String opportunityName) {
		this.opportunityName = opportunityName;
	}

	/**
	 * @return the displayOpportunityName
	 */
	public String getDisplayOpportunityName() {
		return displayOpportunityName;
	}

	/**
	 * @param displayOpportunityName the displayOpportunityName to set
	 */
	public void setDisplayOpportunityName(String displayOpportunityName) {
		this.displayOpportunityName = displayOpportunityName;
	}

	/**
	 * @return the opportunityType
	 */
	public String getOpportunityType() {
		return opportunityType;
	}

	/**
	 * @param opportunityType the opportunityType to set
	 */
	public void setOpportunityType(String opportunityType) {
		this.opportunityType = opportunityType;
	}

	/**
	 * @return the announcementDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getAnnouncementDate() {
		return announcementDate;
	}

	/**
	 * @param announcementDate the announcementDate to set
	 */
	public void setAnnouncementDate(DateTime announcementDate) {
		this.announcementDate = announcementDate;
	}

	/**
	 * @return the submissionBeginDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getSubmissionBeginDate() {
		return submissionBeginDate;
	}

	/**
	 * @param submissionBeginDate the submissionBeginDate to set
	 */
	public void setSubmissionBeginDate(DateTime submissionBeginDate) {
		this.submissionBeginDate = submissionBeginDate;
	}

	/**
	 * @return the submissionEndDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getSubmissionEndDate() {
		return submissionEndDate;
	}

	/**
	 * @param submissionEndDate the submissionEndDate to set
	 */
	public void setSubmissionEndDate(DateTime submissionEndDate) {
		this.submissionEndDate = submissionEndDate;
	}

	/**
	 * @return the submissionURL
	 */
	public String getSubmissionURL() {
		return submissionURL;
	}

	/**
	 * @param submissionURL the submissionURL to set
	 */
	public void setSubmissionURL(String submissionURL) {
		this.submissionURL = submissionURL;
	}

	/**
	 * @return the defaultAllocationAwardPeriod
	 */
	public int getDefaultAllocationAwardPeriod() {
		return defaultAllocationAwardPeriod;
	}

	/**
	 * @param defaultAllocationAwardPeriod the defaultAllocationAwardPeriod to set
	 */
	public void setDefaultAllocationAwardPeriod(int defaultAllocationAwardPeriod) {
		this.defaultAllocationAwardPeriod = defaultAllocationAwardPeriod;
	}

	/**
	 * @return the allocationType
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/**
	 * @param allocationType the allocationType to set
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	@JsonProperty("allocationTypeInfo")
	public AllocationTypeInfo getAllocationTypeInfo() {
		return allocationTypeInfo;
	}

	public void setAllocationTypeInfo(AllocationTypeInfo allocationTypeInfo) {
		this.allocationTypeInfo = allocationTypeInfo;
	}

	/**
	 * @return the resources
	 */
	public List<OpportunityResource> getResources() {
		return resources;
	}

	/**
	 * @param resources the resources to set
	 */
	public void setResources(List<OpportunityResource> resources) {
		this.resources = resources;
	}

	/**
	 * @return the panels
	 */
	public List<OpportunityPanel> getPanels() {
		return panels;
	}

	/**
	 * @param panels the panels to set
	 */
	public void setPanels(List<OpportunityPanel> panels) {
		this.panels = panels;
	}

	/**
	 * @return the opportunityStates
	 */
	public List<String> getOpportunityStates() {
		return opportunityStates;
	}

	/**
	 * @param opportunityStates the opportunityStates to set
	 */
	public void setOpportunityStates(List<String> opportunityStates) {
		this.opportunityStates = opportunityStates;
	}

	/**
	 * @return the attributeSets
	 */
	public List<AttributeSet<OpportunityAttribute>> getAttributeSets() {
		return attributeSets;
	}

	/**
	 * @param attributeSets the attributeSets to set
	 */
	public void setAttributeSets(List<AttributeSet<OpportunityAttribute>> attributeSets) {
		this.attributeSets = attributeSets;
	}

	/**
	 * @return the terminatingOpportunity
	 */
	public OpportunityTermination getTerminatingOpportunity() {
		return terminatingOpportunity;
	}

	/**
	 * @param terminatingOpportunity the terminatingOpportunity to set
	 */
	public void setTerminatingOpportunity(OpportunityTermination terminatingOpportunity) {
		this.terminatingOpportunity = terminatingOpportunity;
	}
	
	/**
   * @return the adminComments
   */
  public String getAdminComments() {
    return adminComments;
  }

  /**
   * @param adminComments the adminComments to set
   */
  public void setAdminComments(String adminComments) {
    this.adminComments = adminComments;
  }

  public String getSubmissionNotes() {
    return submissionNotes;
  }

  public void setSubmissionNotes(String submissionNotes) {
    this.submissionNotes = submissionNotes;
  }

	public int getRelativeOrder() {
		return relativeOrder;
	}

	/**
	@JsonProperty("allocationTypeInfo")
	public void setRelativeOrder(Map<String, Object> allocationTypeInfo) {
		this.relativeOrder = (Integer) allocationTypeInfo.get("relativeOrder");
	}**/

  public static final String BASE_URL = "/opportunities";
	
	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allocationType == null) ? 0 : allocationType.hashCode());
		result = prime * result + ((opportunityName == null) ? 0 : opportunityName.hashCode());
		result = prime * result + ((opportunityType == null) ? 0 : opportunityType.hashCode());
		result = prime * result + ((submissionBeginDate == null) ? 0 : submissionBeginDate.hashCode());
		result = prime * result + ((submissionEndDate == null) ? 0 : submissionEndDate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Opportunity other = (Opportunity) obj;
		if (allocationType == null) {
			if (other.allocationType != null)
				return false;
		} else if (!allocationType.equals(other.allocationType))
			return false;
		if (opportunityName == null) {
			if (other.opportunityName != null)
				return false;
		} else if (!opportunityName.equals(other.opportunityName))
			return false;
		if (opportunityType == null) {
			if (other.opportunityType != null)
				return false;
		} else if (!opportunityType.equals(other.opportunityType))
			return false;
		if (submissionBeginDate == null) {
			if (other.submissionBeginDate != null)
				return false;
		} else if (!submissionBeginDate.equals(other.submissionBeginDate))
			return false;
		if (submissionEndDate == null) {
			if (other.submissionEndDate != null)
				return false;
		} else if (!submissionEndDate.equals(other.submissionEndDate))
			return false;
		return true;
	}

}
