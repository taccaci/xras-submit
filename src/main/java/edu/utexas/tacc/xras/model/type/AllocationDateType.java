/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class AllocationDateType extends Type {

	public AllocationDateType() {
	}

	public AllocationDateType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/allocation_dates";

	private String allocationDateType;

	private String displayAllocationDateType;

	@Override
	@JsonProperty("allocationDateTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the allocationDateType
	 */
	public String getAllocationDateType() {
		return allocationDateType;
	}

	/**
	 * @param allocationDateType
	 *            the allocationDateType to set
	 */
	public void setAllocationDateType(String allocationDateType) {
		this.allocationDateType = allocationDateType;
	}

	/**
	 * @return the displayAllocationDateType
	 */
	public String getDisplayAllocationDateType() {
		return displayAllocationDateType;
	}

	/**
	 * @param displayAllocationDateType
	 *            the displayAllocationDateType to set
	 */
	public void setDisplayAllocationDateType(String displayAllocationDateType) {
		this.displayAllocationDateType = displayAllocationDateType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
