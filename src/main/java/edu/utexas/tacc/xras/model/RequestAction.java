/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestAction extends BaseXrasModel<RequestAction> {

	private int id;

	private String actionType;
	
	private String actionStatus;
	
	private String userComments;
	
	private String adminComments;
	
	private List<String>states = new ArrayList<String>();
	
	private List <RequestResource>resources = new ArrayList<RequestResource>();
	
	private List <RequestDocument>documents = new ArrayList<RequestDocument>();
	
	private List <Review>reviews = new ArrayList<Review>();
	
	private List <RequestAllocationDate>allocationDates = new ArrayList<RequestAllocationDate>();
	
	private List <OpportunityAttributeValue>opportunityAttributes = new ArrayList<OpportunityAttributeValue>();
	
	private List <ResourceAttributeValue>resourceAttributes = new ArrayList<ResourceAttributeValue>();

	private boolean deleted;

	/**
	 * Not entirely sure on the types here, just adding them to clean up logs
	 */
	private String finalReview;
	private List<String> collaborators;
	private List<Review> finalReviews = new ArrayList<>();
	
	/**
	 * @return the id
	 */
	@JsonProperty("actionId")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the actionStatus
	 */
	public String getActionStatus() {
		return actionStatus;
	}

	/**
	 * @param actionStatus the actionStatus to set
	 */
	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	/**
	 * @return the userComments
	 */
	public String getUserComments() {
		return userComments;
	}

	/**
	 * @param userComments the userComments to set
	 */
	public void setUserComments(String userComments) {
		this.userComments = userComments;
	}

	/**
	 * @return the adminComments
	 */
	public String getAdminComments() {
		return adminComments;
	}

	/**
	 * @param adminComments the adminComments to set
	 */
	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	/**
	 * @return the states
	 */
	public List<String> getStates() {
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public void setStates(List<String> states) {
		this.states = states;
	}

	/**
	 * @return the resources
	 */
	public List<RequestResource> getResources() {
		return resources;
	}

	/**
	 * @param resources the resources to set
	 */
	public void setResources(List<RequestResource> resources) {
		this.resources = resources;
	}

	/**
	 * @return the documents
	 */
	public List<RequestDocument> getDocuments() {
		return documents;
	}

	/**
	 * @param documents the documents to set
	 */
	public void setDocuments(List<RequestDocument> documents) {
		this.documents = documents;
	}

	/**
	 * @return the reviews
	 */
	public List<Review> getReviews() {
		return reviews;
	}

	/**
	 * @param reviews the reviews to set
	 */
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	/**
	 * @return the allocationDates
	 */
	public List<RequestAllocationDate> getAllocationDates() {
		return allocationDates;
	}

	/**
	 * @param allocationDates the allocationDates to set
	 */
	public void setAllocationDates(List<RequestAllocationDate> allocationDates) {
		this.allocationDates = allocationDates;
	}

	/**
	 * @return the opportunityAttributes
	 */
	public List<OpportunityAttributeValue> getOpportunityAttributes() {
		return opportunityAttributes;
	}

	/**
	 * @param opportunityAttributes the opportunityAttributes to set
	 */
	public void setOpportunityAttributes(List<OpportunityAttributeValue> opportunityAttributes) {
		this.opportunityAttributes = opportunityAttributes;
	}

	/**
	 * @return the resourceAttributes
	 */
	public List<ResourceAttributeValue> getResourceAttributes() {
		return resourceAttributes;
	}

	/**
	 * @param resourceAttributes the resourceAttributes to set
	 */
	public void setResourceAttributes(List<ResourceAttributeValue> resourceAttributes) {
		this.resourceAttributes = resourceAttributes;
	}

	public String getFinalReview() {
		return finalReview;
	}

	public void setFinalReview(String finalReview) {
		this.finalReview = finalReview;
	}

	public List<String> getCollaborators() {
		return collaborators;
	}

	public void setCollaborators(List<String> collaborators) {
		this.collaborators = collaborators;
	}

	public List<Review> getFinalReviews() {
		return finalReviews;
	}

	public void setFinalReviews(List<Review> finalReviews) {
		this.finalReviews = finalReviews;
	}

	/**
	 * @return the deleted
	 */
	@JsonProperty("isDeleted")
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/actions";
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#getInstanceUrl()
	 */
	@Override
	public String getInstanceUrl() {
		if (getId() == 0) {
			return getBaseUrl();
		} else {
			return getBaseUrl() + "/" + getId();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime + getId();
		result = prime * result + ((actionType == null) ? 0 : actionType.hashCode());
		result = prime * result + ((actionStatus == null) ? 0 : actionStatus.hashCode());
		
		for (RequestAllocationDate date : allocationDates) {
			result = prime * result + date.hashCode();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestAction other = (RequestAction) obj;
		if (getId() != other.getId())
			return false;
		if (actionType == null) {
			if (other.actionType != null)
				return false;
		} else if (!actionType.equals(other.actionType))
			return false;
		if (actionStatus == null) {
			if (other.actionStatus != null)
				return false;
		} else if (!actionStatus.equals(other.actionStatus))
			return false;
		if (allocationDates.size() != other.allocationDates.size()) {
			return false;
		} else {
			for (RequestAllocationDate date : allocationDates) {
				if (! other.allocationDates.contains(date)) {
					return false;
				}
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#toJSON()
	 */
	@Override
	public ObjectNode toJSON() {
		ObjectNode json = getObjectMapper().createObjectNode();
		json.put("actionType", actionType);
		json.put("userComments", userComments);
		return json;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#toJSON(java.lang.Class[])
	 */
	@Override
	public ObjectNode toJSON(Class<?>... mixins) {
		return toJSON();
	}

}
