/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class RoleType extends Type {

	public RoleType() {
	}

	public RoleType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/roles";

	private String roleType;

	private String displayRoleType;

	private boolean isActive;
	private int relativeOrder;

	@Override
	@JsonProperty("roleTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the roleType
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType
	 *            the roleType to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * @return the displayRoleType
	 */
	public String getDisplayRoleType() {
		return displayRoleType;
	}

	/**
	 * @param displayRoleType
	 *            the displayRoleType to set
	 */
	public void setDisplayRoleType(String displayRoleType) {
		this.displayRoleType = displayRoleType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

	public boolean isActive() {
		return isActive;
	}
	
	@JsonProperty("isActive")
	public void setActive(boolean active) {
		isActive = active;
	}

	public int getRelativeOrder() {
		return relativeOrder;
	}

	public void setRelativeOrder(int relativeOrder) {
		this.relativeOrder = relativeOrder;
	}
}
