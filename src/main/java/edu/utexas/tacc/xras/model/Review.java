/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class Review extends BaseXrasModel<Review> {
	
	public static final String BASE_URL = "/reviews";
	
	private DateTime dateReviewed;
	
	private String review;
	
	private String researchPlan;
	
	private String methodology;
	
	private String resourcesUse;
	
	private String overallRating;
	
	private List<ReviewResource> resources = new ArrayList<ReviewResource>();

	private String title;
	private String text;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the id
	 */
	@JsonProperty("reviewId")
	public int getId() {
		return id;
	}

	/**
	 * @return the dateReviewed
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getDateReviewed() {
		return dateReviewed;
	}

	/**
	 * @param dateReviewed the dateReviewed to set
	 */
	public void setDateReviewed(DateTime dateReviewed) {
		this.dateReviewed = dateReviewed;
	}

	/**
	 * @return the review
	 */
	public String getReview() {
		return review;
	}

	/**
	 * @param review the review to set
	 */
	public void setReview(String review) {
		this.review = review;
	}

	/**
	 * @return the researchPlan
	 */
	public String getResearchPlan() {
		return researchPlan;
	}

	/**
	 * @param researchPlan the researchPlan to set
	 */
	public void setResearchPlan(String researchPlan) {
		this.researchPlan = researchPlan;
	}

	/**
	 * @return the methodology
	 */
	public String getMethodology() {
		return methodology;
	}

	/**
	 * @param methodology the methodology to set
	 */
	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	/**
	 * @return the resourcesUse
	 */
	public String getResourcesUse() {
		return resourcesUse;
	}

	/**
	 * @param resourcesUse the resourcesUse to set
	 */
	public void setResourcesUse(String resourcesUse) {
		this.resourcesUse = resourcesUse;
	}

	/**
	 * @return the overallRating
	 */
	public String getOverallRating() {
		return overallRating;
	}

	/**
	 * @param overallRating the overallRating to set
	 */
	public void setOverallRating(String overallRating) {
		this.overallRating = overallRating;
	}

	/**
	 * @return the resources
	 */
	public List<ReviewResource> getResources() {
		return resources;
	}

	/**
	 * @param resources the resources to set
	 */
	public void setResources(List<ReviewResource> resources) {
		this.resources = resources;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Review other = (Review) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
