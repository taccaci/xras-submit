/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpportunityResource extends BaseModel {

	private int id;
	
	private String resourceName;
	
	private String displayResourceName;
	
	private String resourceType;
	
	private String resourceState;
	
	private String siteProviderComments;
	
	private List<OpportunityResourceNumber> numbers = new ArrayList<OpportunityResourceNumber>();
	
	private List<ResourceDependency> requiredResources = new ArrayList<ResourceDependency>();

	/**
	 * Just setting these to get it to stop puking in my logs
	 */
	private String resourceUnitType;

	private String resourceUnits;

	private String resourceActualType;

	private Integer relativeOrder;

	/**
	 * @return the id
	 */
	@JsonProperty("resourceId")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the displayResourceName
	 */
	public String getDisplayResourceName() {
		return displayResourceName;
	}

	/**
	 * @param displayResourceName the displayResourceName to set
	 */
	public void setDisplayResourceName(String displayResourceName) {
		this.displayResourceName = displayResourceName;
	}

	/**
   * @return the resourceType
   */
  public String getResourceType() {
    return resourceType;
  }

  /**
   * @param resourceType the resourceType to set
   */
  public void setResourceType(String resourceType) {
    this.resourceType = resourceType;
  }

  /**
	 * @return the resourceState
	 */
	public String getResourceState() {
		return resourceState;
	}

	/**
	 * @param resourceState the resourceState to set
	 */
	public void setResourceState(String resourceState) {
		this.resourceState = resourceState;
	}

	/**
	 * @return the siteProviderComments
	 */
	public String getSiteProviderComments() {
		return siteProviderComments;
	}

	/**
	 * @param siteProviderComments the siteProviderComments to set
	 */
	public void setSiteProviderComments(String spComments) {
		this.siteProviderComments = spComments;
	}

	/**
	 * @return the numbers
	 */
	public List<OpportunityResourceNumber> getNumbers() {
		return numbers;
	}

	/**
	 * @param numbers the numbers to set
	 */
	public void setNumbers(List<OpportunityResourceNumber> numbers) {
		this.numbers = numbers;
	}

	/**
	 * @return the requiredResources
	 */
	public List<ResourceDependency> getRequiredResources() {
		return requiredResources;
	}

	/**
	 * @param requiredResources the requiredResources to set
	 */
	public void setRequiredResources(List<ResourceDependency> requiredResources) {
		this.requiredResources = requiredResources;
	}

	public String getResourceUnitType() {
		return resourceUnitType;
	}

	public void setResourceUnitType(String resourceUnitType) {
		this.resourceUnitType = resourceUnitType;
	}

	public String getResourceUnits() {
		return resourceUnits;
	}

	public void setResourceUnits(String resourceUnits) {
		this.resourceUnits = resourceUnits;
	}

	public String getResourceActualType() {
		return resourceActualType;
	}

	public void setResourceActualType(String resourceActualType) {
		this.resourceActualType = resourceActualType;
	}

	public Integer getRelativeOrder() {
		return relativeOrder;
	}

	public void setRelativeOrder(Integer relativeOrder) {
		this.relativeOrder = relativeOrder;
	}
}
