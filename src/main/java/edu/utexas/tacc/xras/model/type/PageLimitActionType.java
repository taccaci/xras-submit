package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by carrie on 6/13/17.
 */
public class PageLimitActionType {

    private int actionTypeId;
    private String actionType;
    private int hardPageLimit;

    @JsonProperty("isRequired")
    private boolean isRequired = true;

    @JsonProperty("allowMany")
    private boolean allowMany = false;

    public int getActionTypeId() {

        return actionTypeId;
    }

    public void setActionTypeId(int actionTypeId) {
        this.actionTypeId = actionTypeId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public int getHardPageLimit() {
        return hardPageLimit;
    }

    public void setHardPageLimit(int hardPageLimit) {
        this.hardPageLimit = hardPageLimit;
    }

    public boolean getIsRequried() { return isRequired; }

    public void setIsRequired(boolean required) { this.isRequired = required; }

    public boolean isAllowMany() {
        return allowMany;
    }

    public void setAllowMany(boolean allowMany) {
        this.allowMany = allowMany;
    }
}
