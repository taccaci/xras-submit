/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * This property of an attribute set describes the type of interface control
 * that an attribute set is requesting on the form.
 * @author mrhanlon
 * 
 */
public class AttributeSetRelationType extends Type {

	public AttributeSetRelationType() {
	}

	public AttributeSetRelationType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/attribute_set_relations";

	private String attributeSetRelationType;

	private String displayAttributeSetRelationType;

	@Override
	@JsonProperty("attributeSetRelationTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the attributeSetRelationType
	 */
	public String getAttributeSetRelationType() {
		return attributeSetRelationType;
	}

	/**
	 * @param attributeSetRelationType
	 *            the attributeSetRelationType to set
	 */
	public void setAttributeSetRelationType(String attributeSetRelationType) {
		this.attributeSetRelationType = attributeSetRelationType;
	}

	/**
	 * @return the displayAttributeSetRelationType
	 */
	public String getDisplayAttributeSetRelationType() {
		return displayAttributeSetRelationType;
	}

	/**
	 * @param displayAttributeSetRelationType
	 *            the displayAttributeSetRelationType to set
	 */
	public void setDisplayAttributeSetRelationType(String displayAttributeSetRelationType) {
		this.displayAttributeSetRelationType = displayAttributeSetRelationType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
