/**
 * 
 */
package edu.utexas.tacc.xras.model;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class OpportunityTermination extends BaseModel {

	private DateTime awardDate;
	
	private DateTime reviewDate;
	
	private int gracePeriod;
	
	private DateTime reviewsDueByDate;
	
	private String adminComments;

	/**
	 * @return the awardDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getAwardDate() {
		return awardDate;
	}

	/**
	 * @param awardDate the awardDate to set
	 */
	public void setAwardDate(DateTime awardDate) {
		this.awardDate = awardDate;
	}

	/**
	 * @return the reviewDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getReviewDate() {
		return reviewDate;
	}

	/**
	 * @param reviewDate the reviewDate to set
	 */
	public void setReviewDate(DateTime reviewDate) {
		this.reviewDate = reviewDate;
	}

	/**
	 * @return the gracePeriod
	 */
	public int getGracePeriod() {
		return gracePeriod;
	}

	/**
	 * @param gracePeriod the gracePeriod to set
	 */
	public void setGracePeriod(int gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	/**
	 * @return the reviewsDueByDate
	 */
	public DateTime getReviewsDueByDate() {
		return reviewsDueByDate;
	}

	/**
	 * @param reviewsDueByDate the reviewsDueByDate to set
	 */
	public void setReviewsDueByDate(DateTime reviewsDueByDate) {
		this.reviewsDueByDate = reviewsDueByDate;
	}

	/**
	 * @return the adminComments
	 */
	public String getAdminComments() {
		return adminComments;
	}

	/**
	 * @param adminComments the adminComments to set
	 */
	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}
}
