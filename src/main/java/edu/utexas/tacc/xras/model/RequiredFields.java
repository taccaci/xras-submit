package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.List;

public class RequiredFields extends BaseXrasModel<RequiredFields> {

    public static final String BASE_URL = "/allocation_types";
    private String actionType;
    private String allocationType;
    private int actionTypeId;
    private int allocationTypeId;

    /**
     * may figure out a better way to do this later!
     * currently we're just shoving this into a loose collection
     * since it is an object with a bunch of sub-objects of
     * varying depths and is theoretically fully dynamic.
     * presumably the javascript front-end is better capable of
     * dealing with it.
     */

    private HashMap<String, Object> requiredFields;

    public HashMap<String, Object> getRequiredFields() {
        return requiredFields;
    }

    public void setRequiredFields(HashMap<String, Object> requiredFields) {
        this.requiredFields = requiredFields;
    }

    @Override
    public boolean equals(Object object) {
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    /*
        this data structure doesn't have or need an actual ID so we're just using the allocation type id
     */
    @Override
    public int getId() {
        return this.allocationTypeId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getAllocationType() {
        return allocationType;
    }

    public void setAllocationType(String allocationType) {
        this.allocationType = allocationType;
    }

    public int getActionTypeId() {
        return actionTypeId;
    }

    public void setActionTypeId(int actionTypeId) {
        this.actionTypeId = actionTypeId;
    }

    public int getAllocationTypeId() {
        return allocationTypeId;
    }

    public void setAllocationTypeId(int allocationTypeId) {
        this.allocationTypeId = allocationTypeId;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    /**
     * We don't use this in the service because it's clunky but still putting it here for consistancy
     * @return
     */
    @Override
    public String getInstanceUrl() {
        return this.getBaseUrl() + "/" + this.getId() + "/action_types/" + this.getActionTypeId() + "required_fields?mode=full";
    }

}
