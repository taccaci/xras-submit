/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class RequestValidation extends BaseModel {

  private String validation;
  
  private List<String> errors;

  /**
   * @return the validation
   */
  public String getValidation() {
    return validation;
  }

  /**
   * @param validation the validation to set
   */
  public void setValidation(String validation) {
    this.validation = validation;
  }

  /**
   * @return the errors
   */
  public List<String> getErrors() {
    return errors;
  }

  /**
   * @param errors the errors to set
   */
  public void setErrors(List<String> errors) {
    this.errors = errors;
  }
  
  
}
