package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestRequiredDocument extends BaseXrasModel<RequestRequiredDocument> {

    private boolean allowMany;
    private String displayDocumentType;
    private String documentType;
    private int documentTypeId;

    // same as document type but just being redundant for the sake of consistency
    private int id;
    private int hardPageLimit;
    private String message;
    private boolean requirementMet;

    public RequestRequiredDocument() {

    }

    public RequestRequiredDocument(int documentTypeId) { super(documentTypeId);}

    public boolean isAllowMany() {
        return allowMany;
    }

    public void setAllowMany(boolean allowMany) {
        this.allowMany = allowMany;
    }

    public String getDisplayDocumentType() {
        return displayDocumentType;
    }

    public void setDisplayDocumentType(String displayDocumentType) {
        this.displayDocumentType = displayDocumentType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public int getHardPageLimit() {
        return hardPageLimit;
    }

    public void setHardPageLimit(int hardPageLimit) {
        this.hardPageLimit = hardPageLimit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isRequirementMet() {
        return requirementMet;
    }

    public void setRequirementMet(boolean requirementMet) {
        this.requirementMet = requirementMet;
    }

    @Override
    public boolean equals(Object object) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    @JsonProperty("documentTypeId")
    public int getId() {
        return id;
    }


    private String BASE_URL = "/required_documents_status";
    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
