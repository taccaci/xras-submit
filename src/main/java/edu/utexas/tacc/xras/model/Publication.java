/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author mrhanlon
 *
 */
public class Publication extends BaseModel {
	
	public Publication() {}
	
	public Publication(String json) throws JsonProcessingException, IOException {
		new ObjectMapper().readerForUpdating(this).readValue(json);
	}

	private int id;
	private String title;
	private String doi;
	private String pubMedId;
	private String bibtex;
	private int year;
	private int month;
	private boolean peerReviewed;
	private PublicationType type;
	private String abstractText;
	private String createdBy;
	private DateTime created;
	private String modifiedBy;
	private DateTime modified;
	private List<PublicationProject> projects = new ArrayList<PublicationProject>();
	private List<PublicationAuthor> authors = new ArrayList<PublicationAuthor>();
	private List<PublicationData> data = new ArrayList<PublicationData>();
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * @param doi the doi to set
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * @return the pubMedId
	 */
	public String getPubMedId() {
		return pubMedId;
	}

	/**
	 * @param pubMedId the pubMedId to set
	 */
	public void setPubMedId(String pubMedId) {
		this.pubMedId = pubMedId;
	}

	/**
	 * @return the bibtex
	 */
	public String getBibtex() {
		return bibtex;
	}

	/**
	 * @param bibtex the bibtex to set
	 */
	public void setBibtex(String bibtex) {
		this.bibtex = bibtex;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the peerReviewed
	 */
	public boolean isPeerReviewed() {
		return peerReviewed;
	}

	/**
	 * @param peerReviewed the peerReviewed to set
	 */
	public void setPeerReviewed(boolean peerReviewed) {
		this.peerReviewed = peerReviewed;
	}

	/**
	 * @return the type
	 */
	public PublicationType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(PublicationType type) {
		this.type = type;
	}

	/**
	 * @return the abstractText
	 */
	public String getAbstractText() {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the created
	 */
	public DateTime getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(DateTime created) {
		this.created = created;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modified
	 */
	public DateTime getModified() {
		return modified;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(DateTime modified) {
		this.modified = modified;
	}

	/**
	 * @return the projects
	 */
	public List<PublicationProject> getProjects() {
		return projects;
	}

	/**
	 * @param projects the projects to set
	 */
	public void setProjects(List<PublicationProject> projects) {
		this.projects = projects;
	}

	/**
	 * @return the authors
	 */
	public List<PublicationAuthor> getAuthors() {
		return authors;
	}

	/**
	 * @param authors the authors to set
	 */
	public void setAuthors(List<PublicationAuthor> authors) {
		this.authors = authors;
	}

	/**
	 * @return the data
	 */
	public List<PublicationData> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<PublicationData> data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getId();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Publication other = (Publication) obj;
		if (doi == null) {
			if (other.doi != null) {
				return false;
			}
		} else if (!doi.equals(other.doi)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (year != other.year) {
			return false;
		}
		return true;
	}

}
