/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * Available actions:
 * <ul>
 * <li>New
 * <li>Renewal
 * <li>Appeal
 * <li>Transfer
 * <li>Extension
 * <li>Advance
 * <li>Supplement
 * <li>Final Report
 * </ul>
 * @author mrhanlon
 * 
 */
public class ActionType extends Type {

	public ActionType() {
	}

	public ActionType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/actions";

	private String actionType;

	private String displayActionType;

	@Override
	@JsonProperty("actionTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the displayActionType
	 */
	public String getDisplayActionType() {
		return displayActionType;
	}

	/**
	 * @param displayActionType
	 *            the displayActionType to set
	 */
	public void setDisplayActionType(String displayActionType) {
		this.displayActionType = displayActionType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
