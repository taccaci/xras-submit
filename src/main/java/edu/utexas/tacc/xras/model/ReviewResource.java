/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import edu.utexas.tacc.xras.model.serializer.BigDecimalDeserializer;

/**
 * @author mrhanlon
 *
 */
public class ReviewResource extends BaseModel {

	private int resourceId;
	
	private BigDecimal suggestedAmount;
	
	private String comments;

	/**
	 * @return the resourceId
	 */
	public int getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}

	/**
	 * @return the suggestedAmount
	 */
	@JsonSerialize(using=ToStringSerializer.class)
	@JsonDeserialize(using=BigDecimalDeserializer.class)
	public BigDecimal getSuggestedAmount() {
		return suggestedAmount;
	}

	/**
	 * @param suggestedAmount the suggestedAmount to set
	 */
	public void setSuggestedAmount(BigDecimal suggestedAmount) {
		this.suggestedAmount = suggestedAmount;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
}
