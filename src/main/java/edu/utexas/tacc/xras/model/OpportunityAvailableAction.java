/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class OpportunityAvailableAction extends BaseModel {
  private String actionType;
  private String requestNumber;
  private int requestId;
  private int actionTypeId;
  private List<Integer> resourceIdsAvailableForAction;
  /**
   * @return the actionType
   */
  public String getActionType() {
    return actionType;
  }
  /**
   * @param actionType the actionType to set
   */
  public void setActionType(String actionType) {
    this.actionType = actionType;
  }
  /**
   * @return the requestNumber
   */
  public String getRequestNumber() {
    return requestNumber;
  }
  /**
   * @param requestNumber the requestNumber to set
   */
  public void setRequestNumber(String requestNumber) {
    this.requestNumber = requestNumber;
  }
  /**
   * @return the requestId
   */
  public int getRequestId() {
    return requestId;
  }
  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(int requestId) {
    this.requestId = requestId;
  }
  /**
   * @return the actionTypeId
   */
  public int getActionTypeId() {
    return actionTypeId;
  }
  /**
   * @param actionTypeId the actionTypeId to set
   */
  public void setActionTypeId(int actionTypeId) {
    this.actionTypeId = actionTypeId;
  }
  /**
   * @return the resourceIdsAvailableForAction
   */
  public List<Integer> getResourceIdsAvailableForAction() {
    return resourceIdsAvailableForAction;
  }
  /**
   * @param resourceIdsAvailableForAction the resourceIdsAvailableForAction to set
   */
  public void setResourceIdsAvailableForAction(List<Integer> resourceIdsAvailableForAction) {
    this.resourceIdsAvailableForAction = resourceIdsAvailableForAction;
  }
}
