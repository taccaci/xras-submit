/**
 * 
 */
package edu.utexas.tacc.xras.service;

import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.http.SubmitClient;
import edu.utexas.tacc.xras.model.XrasModel;

/**
 * @author mrhanlon
 *
 */
public abstract class AbstractXrasService<T extends XrasModel<? super T>> {
	
	@Autowired
	private SubmitClient client;
	
	protected SubmitClient getClient() {
		return client;
	}

}
