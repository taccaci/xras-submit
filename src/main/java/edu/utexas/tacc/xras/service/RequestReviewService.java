/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestReview;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
@Service
public class RequestReviewService extends AbstractXrasService<RequestReview> {
  
  public RequestReview get(XrasSession session, Request request) throws ServiceException {
    try {
      ApiResponse response = getClient().get(request.getInstanceUrl() + "/reviews", session);
      if (response.isOk()) {
        return XrasParser.parseModel(response.getResult(), RequestReview.class);
      } else {
        throw new ServiceException(response.getMessage(), response.getStatusCode());
      }
    } catch (IOException e) {
      throw new ServiceException(e.getMessage(), 500, e);
    }

  }
}
