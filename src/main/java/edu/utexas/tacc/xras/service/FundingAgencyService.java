/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.FundingAgency;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
@Service
public class FundingAgencyService extends AbstractXrasService<FundingAgency> {

	/**
	 * 
	 * A GET for the a collection of models from the API.
	 * @return The collection 
	 * @throws ServiceException If an API error occurs
	 */
	public List<FundingAgency> list(XrasSession session) throws ServiceException {
		try {
			ApiResponse response = getClient().get(new FundingAgency().getBaseUrl(), session);
			if (response.isOk()) {
				return XrasParser.parseList(response.getResult(), FundingAgency.class);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
			throw new ServiceException(e);
		}
	}

}
