/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
@Service
public class PersonService extends AbstractXrasService<Person> {


	/**
	 * 
	 * A GET request for a model.  Fetches/refreshes the model from the API
	 * @param session The XRAS session
	 * @param model The model to fetch
	 * @return The model populated/refreshed from the the API. 
	 * @throws ServiceException If an API error occurs or if the passed model <code>isNew</code> and does not have an id set.
	 */
	public Person read(XrasSession session, Person model) throws ServiceException {
		try {
			ApiResponse response = getClient().get(model.getInstanceUrl(), session);
			if (response.isOk()) {
				return XrasParser.parseModel(response.getResult(), Person.class);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), 500, e);
		}
	}

	/**
	 * Person models are always saved with POST requests
	 */
	public Person save(XrasSession session, Person model) throws ServiceException {
		try {
			ApiResponse response = getClient().post(model.getInstanceUrl(), model.toJSON().toString(), session);
			if (response.isOk()) {
				return XrasParser.parseModel(response.getResult(), Person.class);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), 500, e);
		}
	}
	
	
}
