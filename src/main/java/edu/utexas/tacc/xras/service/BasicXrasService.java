/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;
import java.util.List;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.XrasModel;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
public abstract class BasicXrasService<T extends XrasModel<? super T>> extends AbstractXrasService<T> {

	/**
	 * @param type
	 */
	public BasicXrasService(Class<T> type) {
		this.type = type;
	}
	
	protected Class<T> type;

	/**
	 * 
	 * A GET request for a model.  Fetches/refreshes the model from the API
	 * @param model The model to fetch
	 * @return The model populated/refreshed from the the API. 
	 * @throws ServiceException If an API error occurs or if the passed model <code>isNew</code> and does not have an id set.
	 */
	public T read(XrasSession session, T model) throws ServiceException {
		try {
			ApiResponse response = getClient().get(model.getInstanceUrl(), session);
			if (response.isOk()) {
				return XrasParser.parseModel(response.getResult(), type);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), 500, e);
		}
	}

	/**
	 * 
	 * A GET for the a collection of models from the API.
	 * @return The collection 
	 * @throws ServiceException If an API error occurs
	 */
	public List<T> list(XrasSession session) throws ServiceException {
		try {
			ApiResponse response = getClient().get(type.newInstance().getBaseUrl(), session);
			if (response.isOk()) {
				return XrasParser.parseList(response.getResult(), type);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
      throw new ServiceException(e.getMessage(), 500, e);
		} catch (InstantiationException e) {
      throw new ServiceException(e.getMessage(), 500, e);
    } catch (IllegalAccessException e) {
      throw new ServiceException(e.getMessage(), 500, e);
    }
	}

}
