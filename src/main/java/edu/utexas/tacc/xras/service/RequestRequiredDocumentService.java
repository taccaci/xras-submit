package edu.utexas.tacc.xras.service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.BinaryResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestAction;
import edu.utexas.tacc.xras.model.RequestRequiredDocument;
import edu.utexas.tacc.xras.model.parser.XrasParser;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class RequestRequiredDocumentService extends BasicXrasService<RequestRequiredDocument> {
    private static final Logger logger = Logger.getLogger(RequestDocumentService.class);

    private int requestId;
    private int actionId;
    private String GET_URL = "";

    public RequestRequiredDocumentService() {
        super(RequestRequiredDocument.class);
    }

    public void setParameters(int requestId, int actionId) {
        this.requestId = requestId;
        this.actionId = actionId;

        GET_URL = "/requests/" + requestId + "/actions/" + actionId + "/required_documents_status";
    }

    @Override
    public List<RequestRequiredDocument> list(XrasSession session) throws ServiceException {
        try {
            ApiResponse response = getClient().get(GET_URL, session);
            if (response.isOk()) {
                return XrasParser.parseList(response.getResult(), type);
            } else {
                throw new ServiceException(response.getMessage(), response.getStatusCode());
            }
        } catch (IOException e) {
            throw new ServiceException(e.getMessage(), 500, e);
        }
    }
}
