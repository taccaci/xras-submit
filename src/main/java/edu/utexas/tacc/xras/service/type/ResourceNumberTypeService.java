/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.ResourceNumberType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class ResourceNumberTypeService extends TypeService<ResourceNumberType> {

	public ResourceNumberTypeService() {
		super(ResourceNumberType.class);
	}
}
