/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Type;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
public class TypeService<T extends Type> extends AbstractXrasService<T> {

	public TypeService(Class<T> type) {
		this.type = type;
	}
	
	private Class<T> type;
	
	/**
	 * 
	 * A GET for the a collection of models from the API.
	 * @return The collection 
	 * @throws ServiceException If an API error occurs
	 */
	public List<T> list(XrasSession session) throws ServiceException {
		try {
			T instance = type.newInstance();
			JsonNode node = getClient().get(instance.getBaseUrl(), session).getResult();
			List<T> models = XrasParser.parseList(node, type);
			return models;
		} catch (IOException e) {
			throw new ServiceException(e.getMessage(), 500, e);
		} catch (InstantiationException e) {
      throw new ServiceException(e.getMessage(), 500, e);
    } catch (IllegalAccessException e) {
      throw new ServiceException(e.getMessage(), 500, e);
    }
	}
}
