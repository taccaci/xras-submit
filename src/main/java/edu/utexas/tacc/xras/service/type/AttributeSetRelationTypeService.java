/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.AttributeSetRelationType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class AttributeSetRelationTypeService extends TypeService<AttributeSetRelationType> {

	public AttributeSetRelationTypeService() {
		super(AttributeSetRelationType.class);
	}
}
