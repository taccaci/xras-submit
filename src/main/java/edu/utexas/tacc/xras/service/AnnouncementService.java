package edu.utexas.tacc.xras.service;

import edu.utexas.tacc.xras.model.Announcement;
import org.springframework.stereotype.Service;

@Service
public class AnnouncementService  extends BasicXrasService<Announcement> {
    public AnnouncementService() {
        super(Announcement.class);
    }
}
