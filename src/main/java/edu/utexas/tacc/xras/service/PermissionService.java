/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Permission;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 * 
 */
@Service
public class PermissionService extends AbstractXrasService<Permission> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.utexas.tacc.xras.service.BaseXrasService#list(edu.utexas.tacc.xras
	 * .http.session.XrasSession)
	 */
	public List<Permission> list(XrasSession session) throws ServiceException {
		try {
			Permission instance = new Permission();
			ApiResponse response = getClient().get(instance.getBaseUrl() + "/" + session.getUserContext(), session);
			if (response.isOk()) {
				List<Permission> models = XrasParser.parseList(response.getResult(), Permission.class);
				return models;
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (IOException e) {
      throw new ServiceException(e.getMessage(), 500, e);
		}
	}
}
