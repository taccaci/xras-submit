package edu.utexas.tacc.xras.service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.RequiredFields;
import edu.utexas.tacc.xras.model.parser.XrasParser;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RequiredFieldsService extends AbstractXrasService<RequiredFields> {

    public RequiredFields read(XrasSession session, RequiredFields requiredFields, int allocationTypeId, int actionTypeId) throws ServiceException {
        String GET_URL = requiredFields.getBaseUrl() + "/" + allocationTypeId + "/action_types/" + actionTypeId + "/required_fields";
        try {
            ApiResponse response = getClient().get(GET_URL, session);
            if (response.isOk()) {
                return XrasParser.parseModel(response.getResult(), RequiredFields.class);
            } else {
                throw new ServiceException(response.getMessage(), response.getStatusCode());
            }
        } catch (IOException e) {
            throw new ServiceException(e.getMessage(), 500, e);
        }

    }
}
