/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.RequestType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class RequestTypeService extends TypeService<RequestType> {
	
	public RequestTypeService() {
		super(RequestType.class);
	}

}
