/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.BinaryResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestAction;
import edu.utexas.tacc.xras.model.RequestDocument;

/**
 * @author mrhanlon
 *
 */
@Service
public class RequestDocumentService extends AbstractXrasService<RequestDocument> {

	private static final Logger logger = Logger.getLogger(RequestDocumentService.class);

	public BinaryResponse getDocument(XrasSession session, Request request, RequestAction action, RequestDocument document) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + document.getInstanceUrl();
		try {
			BinaryResponse response = getClient().getBinary(url, session);
	
			if (logger.isDebugEnabled()) {
				logger.debug(response.getContentDisposition());
				logger.debug(response.getContentType());
				logger.debug(response.getResult().length);
			}
			
			return response;
		} catch (IOException e) {
			logger.error(e);
      throw new ServiceException(e.getMessage(), 500, e);
		}
	}
}
