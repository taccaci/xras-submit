/**
 * 
 */
package edu.utexas.tacc.xras.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.XrasModel;

/**
 * @author mrhanlon
 * 
 */
@Component
public class SubmitClient {

	private final boolean ssl;

	private final String scheme;

	private final String host;

	private final String apiBase;

	private final String apiVersion;

	private final String allocationsProcess;

	private final String context;

	private final String apiKey;

	private static final Logger log = Logger.getLogger(SubmitClient.class);
	
	public SubmitClient() throws IOException {
		this(SubmitClient.class.getResourceAsStream("/xras-submit.xml"));
	}

	public SubmitClient(File config) throws IOException {
		this(new FileInputStream(config));
	}

	private SubmitClient(InputStream config) throws IOException {
		Properties props = new Properties();
		try {
			props.loadFromXML(config);
			ssl = Boolean.parseBoolean(props.getProperty("ssl", "true"));
			scheme = ssl ? "https" : "http";
			host = props.getProperty("host");
			apiBase = props.getProperty("api-base");
			apiVersion = props.getProperty("api-version");
			allocationsProcess = props.getProperty("header-allocations-process");
			context = props.getProperty("header-context");
			apiKey = props.getProperty("header-api-key");
		} catch (IOException e) {
			log.error("Error loading XRAS Submit properties", e);
			throw e;
		} finally {
			config.close();
		}
	}

	public SubmitClient(String host, String apiBase, String apiVersion, String allocationsProcess, String context, String apiKey, boolean ssl) {
		this.ssl = ssl;
		this.scheme = ssl ? "https://" : "http://";
		this.host = host;
		this.apiBase = apiBase;
		this.apiVersion = apiVersion;
		this.allocationsProcess = allocationsProcess;
		this.context = context;
		this.apiKey = apiKey;
	}
	
	public ApiResponse get(String path, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		log.info("GET " + uri);
		return doApiRequest(Request.Get(uri), userSession);
	}
	
	public BinaryResponse getBinary(String path, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		log.info("GET " + uri);
		return doBinaryRequest(Request.Get(uri), userSession);
	}

	public ApiResponse post(XrasModel<?> model, XrasSession userSession) throws ClientProtocolException, IOException {
		return post(model.getBaseUrl(), model.toJSON().toString(), userSession);
	}

	public ApiResponse post(String path, String jsonBody, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		//if (log.isDebugEnabled()) {
			log.info("POST " + uri);
			log.info("Request body: " + jsonBody);
		//}
		return doApiRequest(Request.Post(uri).bodyString(jsonBody, ContentType.APPLICATION_JSON), userSession);
	}

	public ApiResponse post(String path, HttpEntity entity, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		//if (log.isDebugEnabled()) {
			log.info("POST " + uri);
			log.info("HttpEntity: " + entity.toString());
		//}
		return doApiRequest(Request.Post(uri).body(entity), userSession);
	}

	public ApiResponse put(XrasModel<?> model, XrasSession userSession) throws ClientProtocolException, IOException {
		return put(model.getInstanceUrl(), model.toJSON().toString(), userSession);
	}

	public ApiResponse put(String path, String jsonBody, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		//if (log.isDebugEnabled()) {
			log.info("PUT " + uri);
			log.info("Request body: " + jsonBody);
		//}
		return doApiRequest(Request.Put(uri).bodyString(jsonBody, ContentType.APPLICATION_JSON), userSession);
	}

	public ApiResponse delete(XrasModel<?> model, XrasSession userSession) throws ClientProtocolException, IOException {
		return delete(model.getInstanceUrl(), userSession);
	}

	public ApiResponse delete(String path, XrasSession userSession) throws ClientProtocolException, IOException {
		String uri = buildUri(path);
		log.info("DELETE " + uri);
		return doApiRequest(Request.Delete(uri), userSession);
	}
	
	protected String buildUri(String path) throws IOException {
	  try {
      return new URI(scheme, host, apiBase + apiVersion + path, null).toASCIIString();
    } catch (URISyntaxException e) {
      throw new IOException(e);
    }
	}
	
	protected ApiResponse doApiRequest(Request request, XrasSession userSession) throws ClientProtocolException, IOException {
		Response response = request
			.addHeader("XA-ALLOCATIONS-PROCESS", allocationsProcess)
			.addHeader("XA-CONTEXT", context)
			.addHeader("XA-API-KEY", apiKey)
			.addHeader("XA-USER", userSession.getUserContext())
			.execute();
		
		return response.handleResponse(new ApiResponseHandler());
	}
	
	protected BinaryResponse doBinaryRequest(Request request, XrasSession userSession) throws ClientProtocolException, IOException {
		Response response = request
			.addHeader("XA-ALLOCATIONS-PROCESS", allocationsProcess)
			.addHeader("XA-CONTEXT", context)
			.addHeader("XA-API-KEY", apiKey)
			.addHeader("XA-USER", userSession.getUserContext())
			.execute();
		
		return response.handleResponse(new BinaryResponseHandler());
	}

//	public String getBaseServiceUrl() {
//		return baseServiceUrl;
//	}

	public boolean isSsl() {
		return ssl;
	}

	public String getScheme() {
		return scheme;
	}

	public String getHost() {
		return host;
	}

	public String getApiBase() {
		return apiBase;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public String getAllocationsProcess() {
		return allocationsProcess;
	}

	public String getContext() {
		return context;
	}

	public String getApiKey() {
		return apiKey;
	}

}
