/**
 * 
 */
package edu.utexas.tacc.xras.http;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author mrhanlon
 *
 */
public class ApiResponseHandler implements ResponseHandler<ApiResponse> {

	private static final Logger logger = Logger.getLogger(ApiResponseHandler.class);
	
	/* (non-Javadoc)
	 * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
	 */
	@Override
	public ApiResponse handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
		StatusLine status = httpResponse.getStatusLine();
		String responseBody = EntityUtils.toString(httpResponse.getEntity());
		
		if (logger.isDebugEnabled()) {
			logger.debug(responseBody);
		}
		
		ApiResponse response = new ApiResponse(status.getStatusCode());
		
		try {
			JsonNode parsed = new ObjectMapper().readTree(responseBody);
			response.setMessage(parsed.get("message").asText());
			response.setResult(parsed.get("result"));
		} catch (Throwable t) {
			response.setMessage(t.getMessage());
		}
		
		return response;
	}

}
