/**
 * 
 */
package edu.utexas.tacc.xras.http;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author mrhanlon
 *
 */
public class ApiResponse {
	
	public ApiResponse(int statusCode) {
		this(statusCode, null);
	}
	
	public ApiResponse(int statusCode, String message) {
		this(statusCode, message, null);
	}
	
	public ApiResponse(int statusCode, String message, JsonNode result) {
		this.statusCode = statusCode;
		this.message = message;
		this.result = result;
	}
	
	private int statusCode;
	
	private String message;
	
	private JsonNode result;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JsonNode getResult() {
		return result;
	}

	public void setResult(JsonNode result) {
		this.result = result;
	}
	
	public boolean isOk() {
		return statusCode >= 200 && statusCode < 300;
	}

}
