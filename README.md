       _  __ ____  ___   _____    _____ __  ______  __  _____________
      | |/ // __ \/   | / ___/   / ___// / / / __ )/  |/  /  _/_  __/
      |   // /_/ / /| | \__ \    \__ \/ / / / __  / /|_/ // /  / /
     /   |/ _, _/ ___ |___/ /   ___/ / /_/ / /_/ / /  / // /  / /
    /_/|_/_/ |_/_/  |_/____/   /____/\____/_____/_/  /_/___/ /_/


#The XSEDE Resource Allocation Service

This is a client library for the XRAS Submit API.  This library implements the following XSEDE XRAS Submit API as documented below.

## Configuration

### Maven

Clone this repo and then install the artifact to your local maven repository.

	$> git clone https://bitbucket.org/taccaci/xras-submit.git
	$> cd xras-submit
	$> mvn install
	
**Note** tests will fail unless you configure your XRAS Submit API credentials (Allocations Process + API key) in `src/test/resources/xras-submit.xml`. An sample configuration is supplied at `src/test/resources/xras-submit-sample.xml`. Simply replace `YOUR_ALLOCATIONS_PROCESS` and `YOUR_API_KEY` with appropriate values. Alternatively, you can add `-DskipTests` to the `mvn install` command to skip the tests.

Add the following dependency to your project:

	<dependency>
		<groupId>edu.utexas.tacc</groupId>
		<artifactId>xras-submit</artifactId>
		<version>1.0</version>
	</dependency>

You will also need to make sure that your XRAS Submit API credentials exists in a file `xras-submit.xml` at the root of the classpath. This is the same file as above needed for tests.

#### xras-submit.xml sample

	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
	<properties>
	  <entry key="host">irg-internal.ncsa.uiuc.edu</entry>
	  <entry key="ssl">true</entry>
	  <entry key="api-base">/xras-api-1</entry>
	  <entry key="api-version">/v1</entry>
	  <entry key="header-allocations-process">YOUR_ALLOCATIONS_PROCESS</entry>
	  <entry key="header-context">submit</entry>
	  <entry key="header-api-key">YOUR_API_KEY</entry>
	</properties>


## XSEDE XRAS Submit API

For canonical, up to date documentation, see [https://xras-submit-api.xsede.org/apidoc/1.0/](https://xras-submit-api.xsede.org/apidoc/1.0/).

---

##[ALLOCATIONS API 1.0](https://xras-submit-api.xsede.org/apidoc/1.0/)


Each HTTP request must have the following request headers set:

|Header|Value|
|-|-|
|XA-ALLOCATIONS-PROCESS: |name, e.g. XSEDE |
|XA-API-KEY:             |api-key |
|XA-USER:                |username of the "person" accessing the API |
|XA-CONTEXT:             |context (one of: submit, review, admin) |

## Resources

### [Funding agencies ](https://xras-submit-api.xsede.org/apidoc/1.0/funding_agencies.html)

|Resource|Description|
|--|--|
|[GET /v1/funding_agencies](https://xras-submit-api.xsede.org/apidoc/1.0/funding_agencies/get.html)|get a list of funding agencies|

### [Opportunities ](https://xras-submit-api.xsede.org/apidoc/1.0/opportunities.html)

|Resource|Description|
|--|--|
|[GET /v1/opportunities](https://xras-submit-api.xsede.org/apidoc/1.0/opportunities/all.html)|get submission opportunities|
|[GET /v1/opportunities/<opportunityId>](https://xras-submit-api.xsede.org/apidoc/1.0/opportunities/by_id.html)|get the specified submission opportunity|

### [Panels ](https://xras-submit-api.xsede.org/apidoc/1.0/panels.html)

|Resource|Description|
|--|--|
|[GET /v1/panels](https://xras-submit-api.xsede.org/apidoc/1.0/panels/all.html)|get a list of panels|
|[GET /v1/panels/<panelId>](https://xras-submit-api.xsede.org/apidoc/1.0/panels/by_id.html)|get the specified panel|

### [People ](https://xras-submit-api.xsede.org/apidoc/1.0/people.html)

|Resource|Description|
|--|--|
|[GET /v1/people/<username>](https://xras-submit-api.xsede.org/apidoc/1.0/people/get.html)|get person info from identity service|
|[POST /v1/people/<username>](https://xras-submit-api.xsede.org/apidoc/1.0/people/post.html)|add or update a person|

### [Permissions ](https://xras-submit-api.xsede.org/apidoc/1.0/permissions.html)

|Resource|Description|
|--|--|
|[GET /v1/permissions/<username>](https://xras-submit-api.xsede.org/apidoc/1.0/permissions/get.html)|get permissions for the specified person|

### [Requests ](https://xras-submit-api.xsede.org/apidoc/1.0/requests.html)

|Resource|Description|
|--|--|
|[GET /v1/requests](https://xras-submit-api.xsede.org/apidoc/1.0/requests/all.html)|get a set of requests|
|[GET /v1/requests/<requestId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/by_id.html)|get the specified request|
|[POST /v1/requests](https://xras-submit-api.xsede.org/apidoc/1.0/requests/create.html)|create a request|
|[POST /v1/requests/<requestId>/roles/<roleType>/<username>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_role.html)|add a role|
|[DELETE /v1/requests/<requestId>/roles/<roleId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_role.html)|delete a role|
|[PUT /v1/requests/<requestId>/roles/<roleId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_role.html)|update a role|
|[POST /v1/requests/<requestId>/actions](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_action.html)|add an action|
|[PUT /v1/requests/<requestId>/actions/<actionId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_action.html)|update an action|
|[DELETE /v1/requests/<requestId>/actions/<actionId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_action.html)|delete an action|
|[PUT /v1/requests/<requestId>/actions/<actionId>/resources/<resourceId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_action_resource.html)|add/update a resource for an action|
|[DELETE /v1/requests/<requestId>/actions/<actionId>/resources/<resourceId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_action_resource.html)|delete a resource from an action|
|[POST /v1/requests/<requestId>/actions/<actionId>/documents](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_action_document.html)|add a document to an action|
|[DELETE /v1/requests/<requestId>/actions/<actionId>/documents/<documentId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_action_document.html)|delete a document from an action|
|[GET /v1/requests/<requestId>/actions/<actionId>/documents/<documentId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/get_action_document.html)|get a document from an action|
|[POST /v1/requests/<requestId>/actions/<actionId>/allocation_dates](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_action_allocation_dates.html)|set allocation dates for an action|
|[GET /v1/requests/<requestId>/reviews](https://xras-submit-api.xsede.org/apidoc/1.0/requests/get_reviews.html)|get the reviews for a request|
|[PUT /v1/requests/<requestId>/actions/<actionId>/allocation_dates/<allocDateId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_action_allocation_dates.html)|update allocation dates for an action|
|[DELETE /v1/requests/<requestId>/actions/<actionId>/allocation_dates/<allocDateId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_action_allocation_dates.html)|delete allocations dates from an action|
|[PUT /v1/requests/<requestId>/actions/<actionId>/opportunity_attributes/<opportunityAttributeId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_action_opportunity_attributes.html)|set opportunity attribute for an action|
|[PUT /v1/requests/<requestId>/actions/<actionId>/resource_attributes/<resourceAttributeId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_action_resource_attributes.html)|set resource attribute for an action|
|[PUT /v1/requests/<requestId>/fos/<fosNum>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_fos.html)|add fos to a request|
|[DELETE /v1/requests/<requestId>/fos/<fosNum>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_fos.html)|delete fos from a request|
|[POST /v1/requests/<requestId>/grants](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_grant.html)|add a supporting grant|
|[PUT /v1/requests/<requestId>/grants/<grantId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_grant.html)|update a supporting grant|
|[DELETE /v1/requests/<requestId>/grants/<grantId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_grant.html)|delete a supporting grant|
|~~[POST /v1/requests/<requestId>/conflicts](https://xras-submit-api.xsede.org/apidoc/1.0/requests/post_conflict.html)~~|~~add a conflict~~|
|~~[PUT /v1/requests/<requestId>/conflicts/<conflictId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_conflict.html)~~|~~update a conflict~~|
|~~[DELETE /v1/requests/<requestId>/conflicts/<conflictId>](https://xras-submit-api.xsede.org/apidoc/1.0/requests/delete_conflict.html)~~|~~delete a conflict~~|
|[PUT /v1/requests/<requestId>/attributes](https://xras-submit-api.xsede.org/apidoc/1.0/requests/put_attributes.html)|add/update request attributes|

### [Resources ](https://xras-submit-api.xsede.org/apidoc/1.0/resources.html)

|Resource|Description|
|--|--|
|[GET /v1/resources](https://xras-submit-api.xsede.org/apidoc/1.0/resources/all.html)|get all resources|
|[GET /v1/resources/<resourceId>](https://xras-submit-api.xsede.org/apidoc/1.0/resources/by_id.html)|get the specified resource|

### [Types ](https://xras-submit-api.xsede.org/apidoc/1.0/types.html)

|Resource|Description|
|--|--|
|[GET /v1/types/fos](https://xras-submit-api.xsede.org/apidoc/1.0/types/fos.html)|get a list of all fields of science|
|[GET /v1/types/documents](https://xras-submit-api.xsede.org/apidoc/1.0/types/documents.html)|get a list of document types|
|[GET /v1/types/actions](https://xras-submit-api.xsede.org/apidoc/1.0/types/actions.html)|get a list of action types|
|[GET /v1/types/roles](https://xras-submit-api.xsede.org/apidoc/1.0/types/roles.html)|get a list of role types|
|[GET /v1/types/permissions](https://xras-submit-api.xsede.org/apidoc/1.0/types/permissions.html)|get a list of permission types|
|[GET /v1/types/conflicts](https://xras-submit-api.xsede.org/apidoc/1.0/types/conflicts.html)|get a list of conflict types|
|[GET /v1/types/requests](https://xras-submit-api.xsede.org/apidoc/1.0/types/requests.html)|get a list of request types|
|[GET /v1/types/request_status](https://xras-submit-api.xsede.org/apidoc/1.0/types/request_status.html)|get a list of request status types|
|[GET /v1/types/units](https://xras-submit-api.xsede.org/apidoc/1.0/types/units.html)|get a list of unit types|
|[POST /v1/types/units](https://xras-submit-api.xsede.org/apidoc/1.0/types/post_units.html)||
|[GET /v1/types/attribute_sets](https://xras-submit-api.xsede.org/apidoc/1.0/types/attribute_sets.html)|get a list of attribute set types|
|[GET /v1/types/attribute_set_relations](https://xras-submit-api.xsede.org/apidoc/1.0/types/attribute_set_relations.html)|get a list of attribute set relation types|
|[GET /v1/types/allocation_dates](https://xras-submit-api.xsede.org/apidoc/1.0/types/allocation_dates.html)|get a list of allocation date types|
|[GET /v1/types/resource_numbers](https://xras-submit-api.xsede.org/apidoc/1.0/types/resource_numbers.html)|get a list of resource number types|
|[GET /v1/types/resource_types](https://xras-submit-api.xsede.org/apidoc/1.0/types/resources.html)|get a list of resource types|
|[GET /v1/types/request_states](https://xras-submit-api.xsede.org/apidoc/1.0/types/request_states.html)|get a list of request state types|
|[GET /v1/types/opportunity_states](https://xras-submit-api.xsede.org/apidoc/1.0/types/opportunity_states.html)|get a list of opportunity state types|

### [Roles - TODO: Route is not documented yet ](https://xras-submit-api.xsede.org/apidoc/1.0/)

|Resource|Description|
|--|--|
|[POST /v1/roles/<requestNumber>/<roleType>/<username> TODO: Route is not documented yet](https://xras-submit-api.xsede.org/apidoc/1.0/)|
|[DELETE /v1/roles/<requestNumber>/<roleType>/<username> TODO: Route is not documented yet](https://xras-submit-api.xsede.org/apidoc/1.0/)|